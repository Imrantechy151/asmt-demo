import { Routes, Route, BrowserRouter } from "react-router-dom"
// import Home from "../pages/home/home-page";
// import About from "../pages/about/about-page";
import AdminLayout from "../layout/Admin/Index";
import TeacherLayout from "../layout/Admin/teacher";
import StudentLayout from "../layout/Admin/student";
import ReviewerLayout from "../layout/Admin/reviewer";
import Quiz from "../pages/Admin/Quiz";
import NotFound from "../pages/Admin/NotFound";
import CreateQuiz from "../pages/Admin/Quiz/Create";
import QuizList from "../pages/Admin/Quiz/index";
import Analytics from "../pages/Admin/analytics";
import CreateQuizTeacher from "../pages/Teacher/Quiz/Create";
import QuizListTeacher from "../pages/Teacher/Quiz/index";
import TestList from "../pages/Teacher/Test/index";
import TestListStudent from "../pages/Student/Test/index";
import AttendTest from "../pages/Student/AttendTest";
import CreateTest from "../pages/Teacher/Test/Create";
import TestDetails from "../pages/Admin/Test/details";
import StartTest from "../pages/Student/starttest";
import Result from "../pages/Student/result";
import Success from "../pages/Student/success";
import QuestionList from "../pages/Reviewer/Quiz/index";
import ReviewerList from "../pages/System Admin/list";
import QuestionType from "../pages/Admin/QuestionType";
import ANACreate from '../pages/Admin/ANA/Create';
import ANAList from '../pages/Admin/ANA/index';
import ANADetails from '../pages/Admin/ANA/details';

function Routing() {
    return (
        <div className="App">
            <BrowserRouter>
                <Routes>
                    <Route path="/" element={<AdminLayout />} >
                        <Route path="/" element={<QuizList />} />
                        <Route path="Admin/Question/Create" element={<CreateQuiz />} />
                        <Route path="Admin/Question" element={<QuizList />} />
                        <Route path="Admin/Analytics" element={<Analytics />} />
                        <Route path="admin/questiontype" element={<QuestionType />} />
                        <Route path="admin/ana" element={<ANAList />} />
                        <Route path="admin/ana/Create" element={<ANACreate />} />
                        <Route path="admin/ana/details/:id"  element={<ANADetails />} />
                        <Route path="*" element={<NotFound />} />
                    </Route>
                    <Route path="/" element={<TeacherLayout />} >
                    <Route path="Teacher/Question/Create" element={<CreateQuizTeacher />} />
                        <Route path="Teacher/Question" element={<QuizListTeacher />} />
                        <Route path="Teacher/Test" element={<TestList />} />
                        <Route path="Teacher/Test/Create" element={<CreateTest />} />
                        <Route path="Teacher/Test/details/:id" element={<TestDetails />} />
                        <Route path="*" element={<NotFound />} />
                    </Route>
                    <Route path="/" element={<StudentLayout />} >
                        <Route path="Student/Test" element={<TestListStudent />} />
                        <Route path="Student/AttendTest/:id" element={<AttendTest />} />
                        <Route path="/Student/Test/Result" element={<Result />} />
                        <Route path="/Student/Test/Success" element={<Success />} />
                        <Route path="*" element={<NotFound />} />
                    </Route>
                    <Route path="/" element={<ReviewerLayout />} >
                        <Route path="/Reviewer/Question" element={<QuestionList />} />
                        <Route path="/SystemAdmin/Reviewer" element={<ReviewerList/>} />
                        <Route path="*" element={<NotFound />} />
                    </Route>
                    <Route path="*" element={<NotFound />} />
                    <Route path="/StartTest" element={<StartTest />} />
                  
                </Routes>

            </BrowserRouter>
        </div>
    )
}

export default Routing