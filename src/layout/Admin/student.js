import React, { Component } from 'react'
import { Outlet, Link } from "react-router-dom";
import Header from '../../component/common-component/header2';
import Sidebar from '../../component/common-component/sidebar';

export default class Student extends Component {
    render() {
        return (
            <>
                <div className='main'>
                    <Sidebar />

                    <div className='content'>
                        <Header />
                        <div className='inner-content'>
                            <Outlet />
                        </div>
                    </div>
                </div>
            </>
        )
    }
}
