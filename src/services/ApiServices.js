import { request } from './Request';
import { Component } from 'react';
//import { routes } from '../constants/constant.route';
// const {
//   GET_USER_INFO
// } = routes.USER;
const BaseURL = "https://1szwrvdlkc.execute-api.ap-south-1.amazonaws.com/"
//const BaseURL = "http://localhost:4000/"
// const BaseURL = "https://bpapi.mimrox.com/"
const API_URL = BaseURL;
//const header = { "content-type": "application/x-www-form-urlencoded" }
export default class ApiService extends Component {

	//GetList
	static GetQuestionBank() {
		return request('GET', API_URL + 'question/bank/list/', null, null, null);
	}
	static GetQuestionBankDetails(id) {
		return request('GET', API_URL + 'question/bank/detail/'+id, null, null, null);
	}
	// Get Details
	static PostQuestion(data) {
		return request('POST', API_URL + 'question/bank/create/', null, data, null);
	}
	//GetList
	static GetExam() {
		return request('GET', API_URL + 'exam/list/', null, null, null);
	}
	//Get Details
	static GetExamDetails(id) {
		return request('GET', API_URL + 'exam/detail/'+id, null, null, null);
	}
	// Create Exam
	static CreateExam(data) {
		return request('POST', API_URL + 'exam/create/', null, data, null);
	}
	// Create Exam Question
	static CreateQuestion(data) {
		return request('POST', API_URL + 'exam/question/insert/', null, data, null);
	}
}