/* eslint-disable no-param-reassign */
/* eslint-disable import/prefer-default-export */
import axios from 'axios';
import qs from 'qs';
//import Storage from './Storage';

/**
 * request interceptors
 * @param {String} method GET,PUT,POST,DELETE
 * @param {String} url req url
 * @param {Object} params query parameters
 * @param {Object} body req body
 * @param {Object} headers req headers
 */
export const request = (method, url, params, body = [], headers = {}) => {
	//const token = localStorage.getItem("token");
//console.log("Headers",headers)
	headers = headers || {};
	params = params || {};
	body = body || {};

	if (!headers['content-type']) {
		headers['content-type'] = 'application/json';
		headers['Access-Control-Allow-Origin'] = '*';
	} else {
		//console.log(headers);
		// if (Storage.getAdminTokenDetail() != null) {
		// 	headers.Authorization = 'Bearer ' + Storage.getAdminTokenDetail();
		// } else {
			//headers.Authorization = 'Bearer ' + Storage.getTokenDetail();
		//}
	}
	const options = {
		method,
		headers,
		params,
		url
	};

	if ((method === 'POST' || method === 'PUT') && headers['content-type'] === 'application/x-www-form-urlencoded') {
		//console.log("header :",headers);
		options.data = qs.stringify(body);
	} else if ((method === 'POST' || method === 'PUT') && headers['content-type'] === 'multipart/form-data') {
		headers['content-type'] = 'multipart/form-data';
		const formData = new FormData();
		const keys = Object.keys(body);
		//console.log(body);
		for (let i = 0; i < keys.length; i++) {
			formData.append(keys[i], body[keys[i]]);
		}
		options.data = formData;
	} else if (method === 'POST' || method === 'PUT') {
		options.data = body;
	}
	//console.log(options);

	return axios(options)
		.then((data) => {
			//	console.log(data);
			return Promise.resolve(data);
		})
		.catch((error) => {
		//	console.log('request error', error.response.data);
			return Promise.reject(error);
		});
};
