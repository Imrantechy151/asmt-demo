import React, { Component } from 'react'
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export default class AlertMessage extends Component {
    static Success(msg) {
        toast.success(msg,
            {
                position: "bottom-center",
                style: { letterSpacing: 1, color: "#434B7B", fontSize: 14 }
            })
    }
    static Error(msg) {
        toast.error(msg,
            {
                position: "bottom-center",
                style: { letterSpacing: 1, color: "#434B7B", fontSize: 14 }
            })
    }
    static Warning(msg) {
        toast.warning(msg,
            {
                position: "bottom-center",
                style: { letterSpacing: 1, color: "#434B7B", fontSize: 14 }
            })
    }
    static info(msg) {
        toast.info(msg,
            {
                position: "bottom-center",
                style: { letterSpacing: 1, color: "#434B7B", fontSize: 14 }
            })
    }
}
