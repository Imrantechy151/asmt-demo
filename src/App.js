import logo from './logo.svg';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Routes from "./router/routes"

function App() {
  return (
   <Routes/>
  );
}

export default App;
