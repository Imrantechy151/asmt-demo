import React, { Component } from 'react';
import {Image} from 'react-bootstrap';
import { Link } from "react-router-dom";

export default class sidebar extends Component {
  render() {
    return (
      <div className='sidebar shadow-sm'>
        <Image src="/images/logo.png" className='img-fluid mb-3 logo' />
        <ul className='menu' style={{marginBottom:25}}>
          <h6 className='small text-theme mb-1' style={{fontWeight:600}}>Content Admin</h6>
          <li><Link to='/admin/question'>Questions</Link></li>  
          <li><Link to='/admin/question/create'>Create Question</Link></li>        
          <li><Link to='/admin/analytics'>Analytics</Link></li>      
          <li><Link to='/admin/questiontype'>Question Types</Link></li>     
          <li><Link to='/admin/ana/create'>Create ANA</Link></li> 
          <li><Link to='/admin/ana'>ANA List</Link></li> 
        </ul>

        <ul className='menu' style={{marginBottom:25}}>
          <h6 className='small text-theme mb-1' style={{fontWeight:600}}>Teacher</h6>
          <li><Link to='/teacher/question'>My Questions List</Link></li>  
          <li><Link to='/teacher/test/create'>Create Test</Link></li>        
          <li><Link to='/teacher/test'>Test List</Link></li>
        </ul>

        <ul className='menu' style={{marginBottom:25}}>
          <h6 className='small text-theme mb-1' style={{fontWeight:600}}>Student</h6>
          <li><Link to='/student/test'>Test List</Link></li>                    
        </ul>

        <ul className='menu' style={{marginBottom:25}}>
          <h6 className='small text-theme mb-1' style={{fontWeight:600}}>Reviewer</h6>
          <li><Link to='/reviewer/question'>Question List</Link></li>                    
        </ul>

        <ul className='menu' style={{marginBottom:20}}>
          <h6 className='small text-theme mb-1' style={{fontWeight:600}}>System Admin</h6>
          <li><Link to='/systemadmin/reviewer'>Reviewer</Link></li>                    
        </ul>

      </div>
    )
  }
}
