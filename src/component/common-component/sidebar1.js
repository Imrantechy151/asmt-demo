import React, { Component } from 'react';
import { Image } from 'react-bootstrap';
import { Link } from "react-router-dom";

export default class sidebar extends Component {
    render() {
        return (
            <div className='sidebar shadow-sm'>
                <Image src="/images/logo.png" className='img-fluid mb-3 logo' />
                <ul className='menu'>
                    <li><Link to='/quiz'>Test Questions</Link></li>
                    {/* <li><Link to='/'>Tests Student Question</Link></li> */}
                    <li><Link to='/Test'>Tests</Link></li>
                    {/* <li><a href='#'>Quiz Type 2</a></li>
          <li><a href='#'>Quiz Type 3</a></li>
          <li><a href='#'>Quiz Type 4</a></li>
          <li><a href='#'>Quiz Type 5</a></li> */}
                </ul>
            </div>
        )
    }
}
