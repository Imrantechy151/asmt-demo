import React, { Component } from 'react'
import { Link } from "react-router-dom";
import { Container, Nav, Navbar, NavDropdown } from 'react-bootstrap';


export default class Header extends Component {
  render() {
    return (
      <div>

        <Navbar expand="lg" className='px-3 py-3'>
          <Container fluid>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="ms-auto profile">
                <div class="dropdown dropstart d-flex align-items-center">
                  
                <div className='text-end'>
                  <span className='small text-secondary'>admin@bestperformance.com</span>
                  <h6 className='mb-0' style={{fontSize:13}}>David Hales (Admin)</h6>
                </div>

                    <button class="circle-profile dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                      A
                    </button>
                   
                  
                  
                  <ul class="dropdown-menu me-0 shadow-sm" aria-labelledby="dropdownMenuButton1" style={{ right: 0 }}>
                    <div class="circle-profile  dropdown-toggle mb-3 d-flex align-items-center justify-content-center mx-auto" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false" style={{ width: 60, height: 60, }}>
                      <span style={{ fontSize: 22 }}>A</span>
                    </div>
                    <h6 className='mb-0'>David Hales</h6>
                    <p className='mb-0 text-secondary small'>(Admin)</p>
                    <span className='small text-secondary'>admin@bestperformance.com</span>
                  </ul>
                </div>

              </Nav>
            </Navbar.Collapse>
          </Container>
        </Navbar>
      </div>
    )
  }
}
