import React, { Component } from 'react'
import { Form, Button, Image, Col, Row, Table, Dropdown } from 'react-bootstrap';
import ApiService from '../../../services/ApiServices';
import AlertMessage from '../../../helper/alertMessage';
import { ThreeDots } from 'react-loader-spinner';
import { Editor } from '@tinymce/tinymce-react';

export default class Quiz_Type_B extends Component {
    constructor(props) {
        super(props)
        this.state = {
            QBMStd: props.std,
            QBMType: props.type,
            QBMSubject: props.subject,
            QBMMarks: 0,
            QBMDQuestionTitle: null,
            QBMDAnswer: null,
            QBMDOperator: props.operator,
            QBMGroup: props.QBMGroup, QBMLearningCode: props.QBMLearningCode,
            QBMExplanation: null,
            QBMTimerValue: null,
            QBMDOperand1: 0,
            QBMDOperand2: 0,
            QBMDDirection: "Horizontal",
            isSubmit: false, ErrorMsg: null,
        }
    }
    componentDidMount() {

    }
    Submit = async (e) => {
        e.preventDefault();
        const { QBMStd, QBMSubject, QBMType, QBMMarks, QBMDQuestionTitle, QBMDAnswer, QBMDOperator, QBMDOperand1, QBMDOperand2, QBMDDirection, QBMGroup, QBMLearningCode, QBMTimerValue, QBMExplanation } = this.state;
        // const { type,std,subject } = this.props
        if (QBMType == "" || QBMStd == "" || QBMSubject == "" || QBMDAnswer == "" || QBMDQuestionTitle == "" || QBMDOperator == "" || QBMDOperand1 == "" || QBMDOperand2 == "" || QBMDDirection == "" || QBMExplanation == null) {
            console.log("All field are required.")
            this.setState({ ErrorMsg: "All field are required." });
            AlertMessage.Error("All Field are Required .")
            return false;
        }
        let body = {
            QBMStd, QBMSubject, QBMType, QBMMarks, QBMDQuestionTitle, QBMDAnswer, QBMDOperator, QBMDOperand1, QBMDOperand2, QBMDDirection, QBMGroup, QBMLearningCode, QBMTimerValue, QBMExplanation
        }
        console.log(body)

        try {
            this.setState({ isSubmit: true })
            await ApiService.PostQuestion(body).then((res) => {
                let data = res.data;
                if (data.status == 200) {
                    this.setState({ isSubmit: false });
                    window.location.href = "/teacher/question";
                }
            }, error => {
                console.log(error);
            })
        }
        catch (error) {
            console.log(error)
        }
    }
    componentDidUpdate(prevProps, nextProps) {
        if (this.props.operator != prevProps.operator) // Check if it's a new user, you can also use some unique property, like the ID  (this.props.user.id !== prevProps.user.id)
        {
            this.setState({ QBMDOperator: this.props.operator, QBMType: this.props.type });
            //console.log(this.props.operator)
        }
        if (this.props.std != prevProps.std) // Check if it's a new user, you can also use some unique property, like the ID  (this.props.user.id !== prevProps.user.id)
        {
            this.setState({ QBMStd: this.props.std });
        }
        if (this.props.subject != prevProps.subject) // Check if it's a new user, you can also use some unique property, like the ID  (this.props.user.id !== prevProps.user.id)
        {
            this.setState({ QBMSubject: this.props.subject });
        }
        if (this.props.QBMTimerValue != prevProps.QBMTimerValue) // Check if it's a new user, you can also use some unique property, like the ID  (this.props.user.id !== prevProps.user.id)
        {
            this.setState({ QBMTimerValue: this.props.QBMTimerValue });
        }
        if (this.props.QBMGroup != prevProps.QBMGroup) // Check if it's a new user, you can also use some unique property, like the ID  (this.props.user.id !== prevProps.user.id)
        {
            this.setState({ QBMGroup: this.props.QBMGroup });
        }
    }
    render() {
        const { QBMDDirection } = this.state
        console.log(this.props.type)
        return (

            <Row className=''>
                <Col md={12}>
                    <Form>
                        <Row>
                            <Col md={12}>
                                <Form.Group className="mb-3">
                                    <Form.Label>Question Title</Form.Label>
                                    <Form.Control type="text" onKeyUp={(e) => { this.setState({ QBMDQuestionTitle: e.target.value }) }} />
                                </Form.Group>
                            </Col>

                            <Col md={12}>
                                <Form.Group className="mb-3">
                                    <Form.Label>Display</Form.Label>
                                    <Form.Select onChange={(e) => {
                                        this.setState({ QBMDDirection: e.target.value })
                                        this.props.ChangeDirection(e.target.value)
                                    }} className='' aria-label="Default select example">
                                        <option selected value="Horizontal">Horizontal</option>
                                        <option value="Vertical">Vertical</option>
                                    </Form.Select>
                                </Form.Group>
                            </Col>

                            <Col md={5}>
                                <Form.Group className="mb-3">
                                    <Form.Label>First Value</Form.Label>
                                    <Form.Control type="number" onKeyUp={(e) => { this.setState({ QBMDOperand1: e.target.value }) }} placeholder='Integer only' />
                                </Form.Group>
                            </Col>
                            <Col md={2}>
                                <Form.Group className="mb-3">
                                    {/* <Form.Label className='text-center d-block'>Operator</Form.Label> */}
                                    <p className='mt-4' style={{ fontSize: 25, textAlign: 'center' }}>{this.props.operator}</p>
                                </Form.Group>
                            </Col>
                            <Col md={5}>
                                <Form.Group className="mb-3">
                                    <Form.Label>Second Value</Form.Label>
                                    <Form.Control type="number" onKeyUp={(e) => { this.setState({ QBMDOperand2: e.target.value }) }} placeholder='Integer only' />
                                </Form.Group>
                            </Col>
                            <Col md={8}>
                                <Form.Group className="mb-3">
                                    <Form.Label>Answer</Form.Label>
                                    <Form.Control type="text" placeholder='Integer Only' onKeyUp={(e) => { this.setState({ QBMDAnswer: e.target.value }) }} />
                                </Form.Group>
                            </Col>
                            <Col md={4}>
                                <Form.Group className="mb-3">
                                    <Form.Label>Marks <span className='text-secondary'>(Optional)</span></Form.Label>
                                    <Form.Control type="number" placeholder='' onKeyUp={(e) => { this.setState({ QBMMarks: e.target.value }) }} />
                                </Form.Group>
                            </Col>


                            <Col md={12}>
                                <Form.Group className="mb-3">
                                    <Form.Label>Explanation</Form.Label>
                                    <Editor
                                        // onInit={(evt, editor) => editorRef.current = editor}
                                        // initialValue=""
                                        onChange={(val) => { this.setState({ QBMExplanation: val.target.getContent() }) }}
                                        apiKey='y7qptd5fo5cu2lqrlfq1xezi948xf0znncpobib4nbvfhmtp'
                                        init={{
                                            height: 250,
                                            menubar: false,
                                            plugins: [
                                                'a11ychecker', 'advlist', 'advcode', 'advtable', 'autolink', 'export',
                                                'lists', 'link', 'charmap', 'preview', 'anchor', 'searchreplace', 'visualblocks',
                                                'powerpaste', 'fullscreen', 'formatpainter', 'insertdatetime', 'media', 'table', 'wordcount'
                                            ],
                                            toolbar: 'undo redo | casechange blocks | bold italic backcolor | ' +
                                                'alignleft aligncenter alignright alignjustify | preview | ' +
                                                'bullist numlist outdent indent | removeformat | table',
                                            content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }',

                                        }}
                                    />
                                </Form.Group>
                            </Col>


                        </Row>


                        <div className='d-flex justify-content-between'>
                            <Button className='btn btn-theme border-0 mt-3 px-4 py-2' type="button" onClick={(e) => { this.Submit(e) }}>
                                {this.state.isSubmit ? (<ThreeDots
                                    height="25"
                                    width="25"
                                    color='#fff'
                                    ariaLabel='loading'
                                />) : "Proceed to Save"}
                            </Button>
                            <Button className='btn btn-theme-secondary border-0 mt-3 px-3 py-2' type="button">
                                Save as Draft
                            </Button>
                        </div>
                    </Form>
                </Col>

            </Row>

        )
    }
}
