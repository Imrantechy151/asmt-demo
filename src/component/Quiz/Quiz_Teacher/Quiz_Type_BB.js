import React, { Component } from 'react'
import { Form, Button, Image, Col, Row, Table, Dropdown } from 'react-bootstrap';
import ApiService from '../../../services/ApiServices';
import AlertMessage from '../../../helper/alertMessage';
import { ThreeDots} from 'react-loader-spinner';

export default class Quiz_Type_B extends Component {
    constructor(props) {
        super(props)
        this.state = {      
            QBMStd: props.std,
            QBMType: props.type,
            QBMSubject: props.subject,
            QBMMarks: 0,
            QBMDQuestionTitle: null,
            QBMDAnswer: null,
            QBMDOperator:null,
            QBMDOperand1:0,
            QBMDOperand2:0,
            QBMDDirection: "Horizontal",                  
            isSubmit: false, ErrorMsg: null,
        }
    }

    Submit = async (e) => {
        e.preventDefault();
        const { QBMStd, QBMSubject, QBMType, QBMMarks, QBMDQuestionTitle, QBMDAnswer, QBMDOperator, QBMDOperand1, QBMDOperand2, QBMDDirection } = this.state;
        // const { type,std,subject } = this.props
        if(QBMType == "" || QBMStd == "" || QBMSubject == "" || QBMDAnswer == "" || QBMDQuestionTitle == "" || QBMMarks == "" || QBMDOperator == "" || QBMDOperand1 == "" || QBMDOperand2 == "" ||  QBMDDirection == "") {
            console.log("All field are required.")
            this.setState({ ErrorMsg: "All field are required." });
            AlertMessage.Error("All Field are Required .")
            return false;
        }
        let body = {
            QBMStd, QBMSubject, QBMType, QBMMarks, QBMDQuestionTitle, QBMDAnswer, QBMDOperator, QBMDOperand1, QBMDOperand2, QBMDDirection
        }
        console.log(body)

        try {
                this.setState({ isSubmit: true })
                await ApiService.PostQuestion(body).then((res) => {
                    let data = res.data;
                    if (data.status == 200) {
                        this.setState({ isSubmit: false });
                        window.location.href = "/teacher/question";
                    }
                },error=>{
                    console.log(error);  
                })
            }
            catch (error) {
                console.log(error)
            }                
    }
    render() {
        const { QBMDDirection } = this.state

        return (

            <Row className=''>
                <Col md={12}>
                    <Form>
                        <Row>
                            <Col md={12}>
                                <Form.Group className="mb-3">
                                    <Form.Label>Question Title</Form.Label>
                                    <Form.Control type="text"  onKeyUp={(e) => { this.setState({ QBMDQuestionTitle: e.target.value }) }}/>
                                </Form.Group>
                            </Col>

                            <Col md={12}>
                                <Form.Group className="mb-3">
                                    <Form.Label>Display</Form.Label>
                                    <Form.Select onChange={(e) => {
                                        this.setState({ QBMDDirection: e.target.value })
                                        this.props.ChangeDirection(e.target.value)
                                    }} className='' aria-label="Default select example">
                                        <option selected value="Horizontal">Horizontal</option>
                                        <option value="Vertical">Vertical</option>
                                    </Form.Select>
                                </Form.Group>
                            </Col>
                        
                                    <Col md={5}>
                                        <Form.Group className="mb-3">
                                            <Form.Label>First Value</Form.Label>
                                            <Form.Control type="number"  onKeyUp={(e) => { this.setState({ QBMDOperand1: e.target.value }) }} placeholder='ex for 100' />
                                        </Form.Group>
                                    </Col>
                                    <Col md={2}>
                                        <Form.Group className="mb-3">
                                            <Form.Label>Operator</Form.Label>
                                            <Form.Select onChange={(e) => { this.setState({ QBMDOperator: e.target.value }) }} className='' aria-label="Default select example">
                                                <option value="+">+</option>
                                                <option value="-">-</option>
                                                <option value="*">*</option>
                                                <option value="/">/</option>
                                            </Form.Select>
                                        </Form.Group>
                                    </Col>
                                    <Col md={5}>
                                        <Form.Group className="mb-3">
                                            <Form.Label>Second Value</Form.Label>
                                            <Form.Control type="number" onKeyUp={(e) => { this.setState({ QBMDOperand2: e.target.value }) }} placeholder='ex for 50' />
                                        </Form.Group>
                                    </Col>
                                    <Col md={6}>
                                        <Form.Group className="mb-3">
                                            <Form.Label>Answer</Form.Label>
                                            <Form.Control type="text" onKeyUp={(e) => { this.setState({ QBMDAnswer: e.target.value }) }} />
                                        </Form.Group>
                                    </Col>

                                    <Col md={6}>
                                        <Form.Group className="mb-3">
                                            <Form.Label>Marks</Form.Label>
                                            <Form.Control type="text" onKeyUp={(e) => { this.setState({ QBMMarks: e.target.value }) }}/>
                                        </Form.Group>
                                    </Col>
                              
                            
                        </Row>

                       

                        <Button className='btn btn-theme border-0 mt-3 px-4 py-2' type="button" onClick={(e) => { this.Submit(e) }}>
                        {this.state.isSubmit ? (<ThreeDots
                                height="25"
                                width="25"
                                color='#fff'
                                ariaLabel='loading'
                            />) : "Proceed to Save"}
                        </Button>
                    </Form>
                </Col>

            </Row>

        )
    }
}
