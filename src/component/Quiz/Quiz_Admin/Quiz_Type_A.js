import React, { Component } from 'react'
import { Form, Button, Image, Col, Row, Table, Dropdown } from 'react-bootstrap';
import AlertMessage from '../../../helper/alertMessage';
import ApiService from '../../../services/ApiServices';
import { ThreeDots } from 'react-loader-spinner'
import { Editor } from '@tinymce/tinymce-react';

export default class Quiz_Type_A extends Component {
    constructor(props) {
        super(props)
        this.state = {
            QBMStd: props.std,
            QBMType: props.type,
            QBMSubject: props.subject,
            QBMMarks: 0, QBMGroup: props.QBMGroup, QBMLearningCode: props.QBMLearningCode,
            QBMDQuestion: null,
            QBMDAnswer: null,
            QBMDWordAfterBlank: null, QBMExplanation: null,
            QBMTimerValue: props.QBMTimerValue,
            // QBMDImage:null,
            isSubmit: false, ErrorMsg: null,

        }
    }
    componentDidMount() {
        console.log(this.props)
    }
    componentDidUpdate(prevProps) {
        if (this.props.std != prevProps.std) // Check if it's a new user, you can also use some unique property, like the ID  (this.props.user.id !== prevProps.user.id)
        {
            this.setState({ QBMStd: this.props.std });
        }
        if (this.props.subject != prevProps.subject) // Check if it's a new user, you can also use some unique property, like the ID  (this.props.user.id !== prevProps.user.id)
        {
            this.setState({ QBMSubject: this.props.subject });
        }
        if (this.props.QBMTimerValue != prevProps.QBMTimerValue) // Check if it's a new user, you can also use some unique property, like the ID  (this.props.user.id !== prevProps.user.id)
        {
            this.setState({ QBMTimerValue: this.props.QBMTimerValue });
        }
        if (this.props.QBMGroup != prevProps.QBMGroup) // Check if it's a new user, you can also use some unique property, like the ID  (this.props.user.id !== prevProps.user.id)
        {
            this.setState({ QBMGroup: this.props.QBMGroup });
        }

    }
    Submit = async (e) => {
        e.preventDefault();
        //this.setState({ isSubmit: true })
        const { QBMType, QBMStd, QBMSubject, QBMDAnswer, QBMDQuestion, QBMMarks, QBMDWordAfterBlank, QBMGroup, QBMLearningCode, QBMTimerValue, QBMExplanation } = this.state;
        let body1 = {
            QBMType, QBMStd, QBMSubject, QBMDAnswer, QBMDQuestionTitle: QBMDQuestion, QBMMarks, QBMDWordAfterBlank, QBMGroup, QBMLearningCode, QBMTimerValue, QBMExplanation
        }
        console.log(body1)
        if (QBMType == "" || QBMStd == "" || QBMSubject == "" || QBMDAnswer == "" || QBMDQuestion == "" || QBMGroup == "" || QBMLearningCode == null || QBMDWordAfterBlank == "" || QBMExplanation == null) {
            //console.log("All field are required.")
            this.setState({ ErrorMsg: "All field are required." });
            AlertMessage.Error("All Field are Required .")
            return false;
        }
        let body = {
            QBMType, QBMStd, QBMSubject, QBMDAnswer, QBMDQuestionTitle: QBMDQuestion, QBMMarks, QBMDWordAfterBlank, QBMGroup, QBMLearningCode, QBMTimerValue, QBMExplanation
        }
        console.log(body)

        if (QBMType && QBMStd && QBMDAnswer && QBMDQuestion && QBMGroup && QBMLearningCode && QBMDWordAfterBlank && QBMExplanation) {
            this.setState({ isSubmit: true })
            try {
                await ApiService.PostQuestion(body).then((res) => {
                    let data = res.data;
                    console.log(data);
                    if (data.status == 200) {
                        this.setState({ isSubmit: false });
                        window.location.href = "/admin/question";
                    }
                }, error => {
                    console.log(error);
                })

            }
            catch (error) {
                console.log(error)
            }
        }
        else {
            AlertMessage.Error("All Fileld Required .")
            // console.log("All Fileld Required .")
        }
    }
    render() {
        const { type } = this.props

        return (



            <Row>

                <Col md={12}>


                    <Form>
                        <Row>
                            <Col md={12}>
                                <Form.Group className="mb-3">
                                    <Form.Label>Question Title</Form.Label>
                                    <Form.Control type="text" placeholder='' onKeyUp={(e) => { this.setState({ QBMDQuestion: e.target.value }) }} />
                                </Form.Group>
                            </Col>
                            <Col md={4}>
                                <Form.Group className="mb-3">
                                    <Form.Label>Answer Box</Form.Label>
                                    <Form.Control type="text" disabled placeholder='' />
                                </Form.Group>
                            </Col>
                            <Col md={5}>
                                <Form.Group className="mb-3">
                                    <Form.Label>Word Beside Answer Box</Form.Label>
                                    <Form.Control type="text" placeholder='' onKeyUp={(e) => { this.setState({ QBMDWordAfterBlank: e.target.value }) }} />
                                </Form.Group>
                            </Col>
                            <Col md={3}>
                                <Form.Group className="mb-3">
                                    <Form.Label>Marks <span className='text-secondary'>(Optional)</span></Form.Label>
                                    <Form.Control type="number" placeholder='' onKeyUp={(e) => { this.setState({ QBMMarks: e.target.value }) }} />
                                </Form.Group>
                            </Col>
                            <Col md={12}>
                                <Form.Group className="mb-3">
                                    <Form.Label>Answer</Form.Label>
                                    <Form.Control type="text" placeholder='Answer (Will be exactly match)' onKeyUp={(e) => { this.setState({ QBMDAnswer: e.target.value }) }} />
                                </Form.Group>
                            </Col>



                            <Col md={12}>
                                <Form.Group className="mb-3">
                                    <Form.Label>Explanation</Form.Label>
                                    <Editor
                                        // onInit={(evt, editor) => editorRef.current = editor}
                                        // initialValue=""
                                        onChange={(val) => { this.setState({ QBMExplanation: val.target.getContent() }) }}
                                        //apiKey='y7qptd5fo5cu2lqrlfq1xezi948xf0znncpobib4nbvfhmtp'
                                        init={{
                                            height: 250,
                                            menubar: false,
                                            plugins: [
                                                'a11ychecker', 'advlist', 'advcode', 'advtable', 'autolink', 'export',
                                                'lists', 'link', 'charmap', 'preview', 'anchor', 'searchreplace', 'visualblocks',
                                                'powerpaste', 'fullscreen', 'formatpainter', 'insertdatetime', 'media', 'table', 'wordcount'
                                            ],
                                            toolbar: 'undo redo | casechange blocks | bold italic backcolor | ' +
                                                'alignleft aligncenter alignright alignjustify | preview | ' +
                                                'bullist numlist outdent indent | removeformat | table',
                                            content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }',

                                        }}
                                    />
                                </Form.Group>
                            </Col>

                        </Row>

                        <div className='d-flex justify-content-between'>
                            <Button className='btn btn-theme border-0 mt-3 px-3 py-2' type="button" onClick={(e) => { this.Submit(e) }}>
                                {this.state.isSubmit ? (<ThreeDots
                                    height="25"
                                    width="25"
                                    color='#fff'
                                    ariaLabel='loading'
                                />) : "Send for Review"}
                            </Button>

                            <Button className='btn btn-theme-secondary border-0 mt-3 px-3 py-2' type="button">
                                Save as Draft
                            </Button>
                        </div>

                    </Form>

                </Col>
            </Row>

        )
    }
}
