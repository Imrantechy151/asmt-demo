import React, { Component } from 'react'
import { Form, Button, Image, Col, Row, Table, Dropdown } from 'react-bootstrap';
import { Editor } from '@tinymce/tinymce-react';
import ApiService from '../../../services/ApiServices';
import AlertMessage from '../../../helper/alertMessage';
import { ThreeDots } from 'react-loader-spinner'
export default class Quiz_Type_D extends Component {
  constructor(props) {
    super(props)
    this.state = {
      type: null,
      QBMStd: props.std,
      QBMType: props.type,
      QBMSubject: props.subject,
      QBMMarks: 0,
      QBMDQuestion: null,
      QBMDAnswer: null,
      QBMDPassage: null,
      QBMDOptionTitle: null,
      option: "2",
      QBMGroup: props.QBMGroup, QBMLearningCode: props.QBMLearningCode,
      QBMExplanation: null,
      QBMDOption1: null,
      QBMDOption2: null,
      QBMDOption3: null,
      QBMDOption4: null,
      QBMDOption5: null,
      QBMDPassageImage: null,
      image: null, imagePreview: null
    }
  }
  onChangeImage(e) {
    let files = e.target.files || e.dataTransfer.files;
    if (!files.length) return;
    console.log("img :", files[0])
    this.setState({
      // image: files[0],
    });
    this.createImage1(files[0])
  }

  createImage1(file) {
    let reader = new FileReader();
    reader.onload = (e) => {
      this.setState({
        imagePreview: e.target.result,
        image: e.target.result
      });
    };
    reader.readAsDataURL(file);
  }
  componentDidUpdate(prevProps) {
    if (this.props.std != prevProps.std) // Check if it's a new user, you can also use some unique property, like the ID  (this.props.user.id !== prevProps.user.id)
    {
      this.setState({ QBMStd: this.props.std });
    }
    if (this.props.subject != prevProps.subject) // Check if it's a new user, you can also use some unique property, like the ID  (this.props.user.id !== prevProps.user.id)
    {
      this.setState({ QBMSubject: this.props.subject });
    }
    if (this.props.QBMGroup != prevProps.QBMGroup) // Check if it's a new user, you can also use some unique property, like the ID  (this.props.user.id !== prevProps.user.id)
    {
      this.setState({ QBMGroup: this.props.QBMGroup });
    }

  }
  Submit = async (e) => {
    e.preventDefault();
    const { QBMStd, QBMSubject, QBMDAnswer, QBMType, QBMMarks, QBMDQuestionTitle, option, QBMDOption1, QBMDOption2, QBMDOption3, QBMDOption4,QBMDOption5, image, QBMDOptionTitle, QBMDPassage, QBMGroup, QBMLearningCode, QBMExplanation } = this.state;
    // const { type,std,subject } = this.props

    let body1 = {
      QBMStd, QBMSubject, QBMType, QBMMarks, QBMDQuestionTitle, QBMDAnswer, QBMDOption: option, QBMDOption1: QBMDOption1, QBMDOption2: QBMDOption2, QBMDOption3: QBMDOption3, QBMDPassage, QBMGroup, QBMLearningCode, QBMExplanation
    }
    console.log(body1)
    if (QBMType == "" || QBMStd == "" || QBMSubject == "" || QBMDAnswer == "" || QBMDQuestionTitle == "" || option == "" || QBMDOption1 == null || QBMDOption2 == null || image == null || QBMDOptionTitle == null || QBMDPassage == null || QBMGroup == null || QBMLearningCode == null || QBMExplanation == "") {
      if (option == "3") {
        if (QBMDOption3 == null || QBMDOption3 == "") {
          AlertMessage.Error("Option 3 is Required");
          return false
        }
      }
      if (option == "4") {
        if (QBMDOption3 == null || QBMDOption3 == "") {
          AlertMessage.Error("Option 3 is Required");
          return false
        }
        if (QBMDOption4 == null || QBMDOption4 == "") {
          AlertMessage.Error("Option 4 is Required");
          return false
        }
      }
      if (option == "5") {
        if (QBMDOption3 == null || QBMDOption3 == "") {
          AlertMessage.Error("Option 3 is Required");
          return false
        }
        if (QBMDOption4 == null || QBMDOption4 == "") {
          AlertMessage.Error("Option 4 is Required");
          return false
        }
        if (QBMDOption5 == null || QBMDOption5 == "") {
          AlertMessage.Error("Option 5 is Required");
          return false
        }
      }
    // console.log("All field are required.")
      this.setState({ ErrorMsg: "All field are required." });
      AlertMessage.Error("All Field are Required .")
      return false;
    }
    // let body = {
    //   QBMStd, QBMSubject, QBMType, QBMMarks, QBMDQuestionTitle, QBMDAnswer, QBMDOption: option, QBMDOption1: QBMDOption1, QBMDOption2: QBMDOption2, QBMDOption3: QBMDOption3
    // }
    // const formData = new FormData();
    // formData.append('QBMStd', QBMStd);
    // formData.append('QBMSubject', QBMSubject);
    // formData.append('QBMType', QBMType);
    // formData.append('QBMMarks', QBMMarks);
    // formData.append('QBMDQuestionTitle', QBMDQuestionTitle);
    // formData.append('QBMDOptionTitle', QBMDOptionTitle);
    // formData.append('QBMDAnswer', QBMDAnswer);
    // formData.append('QBMDPassage', QBMDPassage);
    // formData.append('QBMDOption', option);

    // formData.append('QBMDPassageImage', image);
    // formData.append('QBMDOption1', QBMDOption1);
    // formData.append('QBMDOption2', QBMDOption2);
    // if (option == "3") {
    //   formData.append('QBMDOption3', QBMDOption3);
    // }
    // if (option == "4") {
    //   formData.append('QBMDOption3', QBMDOption3);
    //   formData.append('QBMDOption4', QBMDOption4);
    // }
    // formData.append('QBMGroup', QBMGroup);
    // formData.append('QBMLearningCode', QBMLearningCode);
    // formData.append('QBMExplanation', QBMExplanation);
    // console.log(formData)
    // console.log(body)
    const data = {
      QBMStd: QBMStd,
      QBMSubject: QBMSubject,
      QBMType: QBMType,
      QBMMarks: QBMMarks,
      QBMDQuestionTitle: QBMDQuestionTitle,
      QBMDOptionTitle: QBMDOptionTitle,
      QBMDAnswer: QBMDAnswer,
      QBMDPassage: QBMDPassage,
      QBMDOption: option,
      QBMDPassageImage: image,
      QBMDOption1: QBMDOption1,
      QBMDOption2: QBMDOption2,
      QBMGroup: QBMGroup,
      QBMLearningCode: QBMLearningCode,
      QBMExplanation: QBMExplanation,
    }
    if (option == "3") {
      data.QBMDOption3 = QBMDOption3;
    }
    if (option == "4") {
      data.QBMDOption3 = QBMDOption3;
      data.QBMDOption4 = QBMDOption4;
    }
    if (option == "5") {
      data.QBMDOption3 = QBMDOption3;
      data.QBMDOption4 = QBMDOption4;
      data.QBMDOption5 = QBMDOption5;
    }
    try {
      this.setState({ isSubmit: true })
      await ApiService.PostQuestion(data).then((res) => {
        let data = res.data;
        if (data.status == 200) {
          this.setState({ isSubmit: false });
          window.location.href = "/admin/question";
        }
      }, error => {
        console.log(error);
      })
    }
    catch (error) {
      console.log(error)
    }
  }
  render() {
    const { type, option, image, imagePreview } = this.state
    return (

      <Row>
        <Col md={12}>
          <Form>
            <Row>
              <Col md={12}>
                <Form.Group className="mb-3">
                  <Form.Label>Question Title</Form.Label>
                  <Form.Control type="text" onKeyUp={(e) => { this.setState({ QBMDQuestionTitle: e.target.value }) }} />
                </Form.Group>
              </Col>
              <Col md={12}>
                <Form.Group className="mb-3">
                  <Form.Label>Passage Text</Form.Label>
                  <Editor
                    // onInit={(evt, editor) => editorRef.current = editor}
                    // initialValue=""
                    onChange={(val) => { this.setState({ QBMDPassage: val.target.getContent() }) }}
                    apiKey='y7qptd5fo5cu2lqrlfq1xezi948xf0znncpobib4nbvfhmtp'
                    init={{
                      height: 250,
                      menubar: false,
                      plugins: [
                        'a11ychecker', 'advlist', 'advcode', 'advtable', 'autolink', 'export',
                        'lists', 'link', 'charmap', 'preview', 'anchor', 'searchreplace', 'visualblocks',
                        'powerpaste', 'fullscreen', 'formatpainter', 'insertdatetime', 'media', 'table', 'wordcount'
                      ],
                      toolbar: 'undo redo | casechange blocks | bold italic backcolor | ' +
                        'alignleft aligncenter alignright alignjustify | preview | ' +
                        'bullist numlist outdent indent | removeformat | table',
                      content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }',

                    }}

                  />
                </Form.Group>
              </Col>
              <Col md={12}>
                <Form.Group className="mb-3">
                  <Form.Label>Passage Image</Form.Label>
                  <div class="dropZoneContainer" style={{ position: 'relative' }}>
                    <input type="file" id="PRDImage" onChange={(e) => this.onChangeImage(e)} name="PRDImage" class="FileUpload" accept=".jpg,.png,.gif" />
                    <div class="dropZoneOverlay text-black w-100">{imagePreview != null ? <img src={imagePreview} className="img-fluid w-100 h-100" style={{ objectFit: 'contain' }} /> : "Upload image"} </div>
                  </div>
                </Form.Group>
              </Col>
              <Col md={9}>
                <Form.Group className="mb-3">
                  <Form.Label>Option Title</Form.Label>
                  <Form.Control type="text" onKeyUp={(e) => { this.setState({ QBMDOptionTitle: e.target.value }) }} />
                </Form.Group>
              </Col>
              <Col md={3}>
                <Form.Group className="mb-3">
                  <Form.Label>Options</Form.Label>
                  <Form.Select onChange={(e) => { this.setState({ option: e.target.value }) }} className='' aria-label="Default select example">
                    <option value="">Select</option>
                    <option selected value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                  </Form.Select>
                </Form.Group>
              </Col>
              {option == "2" ?
                <>
                  <Col md={12}>
                    <Form.Group className="mb-3">
                      <Form.Label>Option 1</Form.Label>
                      <Form.Control type="text" onKeyUp={(e) => { this.setState({ QBMDOption1: e.target.value }) }} />
                    </Form.Group>
                  </Col>
                  <Col md={12}>
                    <Form.Group className="mb-3">
                      <Form.Label>Option 2</Form.Label>
                      <Form.Control type="text" onKeyUp={(e) => { this.setState({ QBMDOption2: e.target.value }) }} />
                    </Form.Group>
                  </Col>
                </> : null
              }
              {option == "3" ?
                <>
                  <Col md={12}>
                    <Form.Group className="mb-3">
                      <Form.Label>Option 1</Form.Label>
                      <Form.Control type="text" onKeyUp={(e) => { this.setState({ QBMDOption1: e.target.value }) }} />
                    </Form.Group>
                  </Col>
                  <Col md={12}>
                    <Form.Group className="mb-3">
                      <Form.Label>Option 2</Form.Label>
                      <Form.Control type="text" onKeyUp={(e) => { this.setState({ QBMDOption2: e.target.value }) }} />
                    </Form.Group>
                  </Col>
                  <Col md={12}>
                    <Form.Group className="mb-3">
                      <Form.Label>Option 3</Form.Label>
                      <Form.Control type="text" onKeyUp={(e) => { this.setState({ QBMDOption3: e.target.value }) }} />
                    </Form.Group>
                  </Col>
                </> : null
              }
              {option == "4" ?
                <>
                  <Col md={12}>
                    <Form.Group className="mb-3">
                      <Form.Label>Option 1</Form.Label>
                      <Form.Control type="text" onKeyUp={(e) => { this.setState({ QBMDOption1: e.target.value }) }} />
                    </Form.Group>
                  </Col>
                  <Col md={12}>
                    <Form.Group className="mb-3">
                      <Form.Label>Option 2</Form.Label>
                      <Form.Control type="text" onKeyUp={(e) => { this.setState({ QBMDOption2: e.target.value }) }} />
                    </Form.Group>
                  </Col>
                  <Col md={12}>
                    <Form.Group className="mb-3">
                      <Form.Label>Option 3</Form.Label>
                      <Form.Control type="text" onKeyUp={(e) => { this.setState({ QBMDOption3: e.target.value }) }} />
                    </Form.Group>
                  </Col>
                  <Col md={12}>
                    <Form.Group className="mb-3">
                      <Form.Label>Option 4</Form.Label>
                      <Form.Control type="text" onKeyUp={(e) => { this.setState({ QBMDOption4: e.target.value }) }} />
                    </Form.Group>
                  </Col>
                </> : null
              }

{option == "5" ?
                <>
                  <Col md={12}>
                    <Form.Group className="mb-3">
                      <Form.Label>Option 1</Form.Label>
                      <Form.Control type="text" onKeyUp={(e) => { this.setState({ QBMDOption1: e.target.value }) }} />
                    </Form.Group>
                  </Col>
                  <Col md={12}>
                    <Form.Group className="mb-3">
                      <Form.Label>Option 2</Form.Label>
                      <Form.Control type="text" onKeyUp={(e) => { this.setState({ QBMDOption2: e.target.value }) }} />
                    </Form.Group>
                  </Col>
                  <Col md={12}>
                    <Form.Group className="mb-3">
                      <Form.Label>Option 3</Form.Label>
                      <Form.Control type="text" onKeyUp={(e) => { this.setState({ QBMDOption3: e.target.value }) }} />
                    </Form.Group>
                  </Col>
                  <Col md={12}>
                    <Form.Group className="mb-3">
                      <Form.Label>Option 4</Form.Label>
                      <Form.Control type="text" onKeyUp={(e) => { this.setState({ QBMDOption4: e.target.value }) }} />
                    </Form.Group>
                  </Col>
                  <Col md={12}>
                    <Form.Group className="mb-3">
                      <Form.Label>Option 5</Form.Label>
                      <Form.Control type="text" onKeyUp={(e) => { this.setState({ QBMDOption5: e.target.value }) }} />
                    </Form.Group>
                  </Col>
                </> : null
              }
              <Col md={6}>
                <Form.Group className="mb-3">
                  <Form.Label> Answer</Form.Label>
                  {/* <Form.Control type="text" placeholder='Answer' onKeyUp={(e) => { this.setState({ QBMDAnswer: e.target.value }) }} /> */}
                  <Form.Select onChange={(e) => {
                    this.setState({ QBMDAnswer: e.target.value, })
                  }} className='' aria-label="Default select example">

                    <option value="">Select Answer</option>
                    <option value="1">Option 1</option>
                    <option value="2">Option 2</option>
                    {option == 3 ? <option value="3">Option 3</option> : null}
                    {option == 4 ? <><option value="3">Option 3</option> <option value="4">Option 4</option></> : null}
                  </Form.Select>
                </Form.Group>
              </Col>
              <Col md={6}>
                <Form.Group className="mb-3">
                  <Form.Label>Marks <span className='text-secondary'>(Optional)</span></Form.Label>
                  <Form.Control type="number" placeholder='' onKeyUp={(e) => { this.setState({ QBMMarks: e.target.value }) }} />
                </Form.Group>
              </Col>

              <Col md={12}>
                <Form.Group className="mb-3">
                  <Form.Label>Explanation</Form.Label>
                  <Editor
                    // onInit={(evt, editor) => editorRef.current = editor}
                    // initialValue=""
                    onChange={(val) => { this.setState({ QBMExplanation: val.target.getContent() }) }}
                    apiKey='y7qptd5fo5cu2lqrlfq1xezi948xf0znncpobib4nbvfhmtp'
                    init={{
                      height: 250,
                      menubar: false,
                      plugins: [
                        'a11ychecker', 'advlist', 'advcode', 'advtable', 'autolink', 'export',
                        'lists', 'link', 'charmap', 'preview', 'anchor', 'searchreplace', 'visualblocks',
                        'powerpaste', 'fullscreen', 'formatpainter', 'insertdatetime', 'media', 'table', 'wordcount'
                      ],
                      toolbar: 'undo redo | casechange blocks | bold italic backcolor | ' +
                        'alignleft aligncenter alignright alignjustify | preview | ' +
                        'bullist numlist outdent indent | removeformat | table',
                      content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }',

                    }}
                  />
                </Form.Group>
              </Col>
            </Row>
            <div className='d-flex justify-content-between'>
              <Button className='btn btn-theme border-0 mt-3 px-4 py-2' type="button" onClick={(e) => { this.Submit(e) }}>
                {this.state.isSubmit ? (<ThreeDots
                  height="25"
                  width="25"
                  color='#fff'
                  ariaLabel='loading'
                />) : "Proceed to Review"}
              </Button>
              <Button className='btn btn-theme-secondary border-0 mt-3 px-3 py-2' type="button">
                Save as Draft
              </Button>
            </div>

          </Form>
        </Col>
      </Row>

    )
  }
}
