import React, { Component } from 'react';
import { Table, Modal, Form, } from 'react-bootstrap';
import Multiselect from 'multiselect-react-dropdown';

export default class list extends Component {
    constructor(props) {
        super(props)
        this.state = {
            IsModel: false,
            QBMLearningCodeData: [
                { name: "ACEAD ACELY1713", id: "ACEAD ACELY1713" },
                { name: "ACEAD ACELY1660", id: "ACEAD ACELY1660" },
                { name: "ACEAD ACELY1661", id: "ACEAD ACELY1661" }
            ],
        }
    }

    render() {
        const { IsModel } = this.state;
        return (
            <div>
                <div className='card p-3 py-2 shadow-sm border-0 mb-2'>
                    <div className=''>
                        <h5 className='mb-0 text-secondary'>Reviewer List</h5>
                    </div>
                </div>
                <div className='card shadow-sm border-0 mb-2'>

                    <div className='d-flex justify-content-between bg-light p-3 rounded'>
                        <h6 className='mb-0'>Reviewer Name</h6>
                        <h6 className='mb-0'>Leaning Code</h6>
                    </div>

                    <div className='card-body border-0'>
                        <div className='d-flex justify-content-between align-items-center p-3 border-bottom mb-3 position-relative'>
                            <p className='small text-secondary mb-0'>Jonathan Carter</p>
                            <div>
                                <span class="chip mx-1">ACEAD ACELY1713</span>
                                <span class="chip mx-1">ACEAD ACELY1713</span>
                                <a className='edit-data' onClick={() => { this.setState({ IsModel: true }) }}><i className="fa fa-pencil"></i></a>
                            </div>
                        </div>
                        <div className='d-flex justify-content-between align-items-center p-3 border-bottom mb-3 position-relative'>
                            <p className='small text-secondary mb-0'>Adam Smith</p>
                            <div>
                                <a className='edit-data' onClick={() => { this.setState({ IsModel: true }) }}><i className="fa fa-pencil"></i></a>
                            </div>
                        </div>
                        <div className='d-flex justify-content-between align-items-center p-3 border-bottom mb-3 position-relative'>
                            <p className='small text-secondary mb-0'>Greham Gough</p>
                            <div>
                                <span class="chip mx-1">ACEAD ACELY1713</span>
                                <a className='edit-data' onClick={() => { this.setState({ IsModel: true }) }}><i className="fa fa-pencil"></i></a>
                            </div>
                        </div>
                    </div>
                </div>


                <Modal centered show={IsModel} onHide={() => { this.setState({ IsModel: false }) }}>
                    <Modal.Header closeButton>
                        <Modal.Title>
                            <h6 className='mb-0'>Edit Reviewer </h6>
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div>
                            <Form.Group className="mb-2" controlId="formBasicPassword">
                                <Form.Label>Name</Form.Label>
                                <Form.Control type="text" placeholder="" value="Adam Smith" />
                            </Form.Group>
                            <Form.Group className="mb-2" controlId="formBasicEmail">
                                <Form.Label>Year Level</Form.Label>
                                <Form.Select onChange={(e) => { this.setState({ std: e.target.value }) }} className='' aria-label="Default select example">
                                    <option value="">Select Year Level</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </Form.Select>
                            </Form.Group>
                            <Form.Group className="mb-2" controlId="formBasicEmail">
                                <Form.Label>Subject</Form.Label>
                                <Form.Select onChange={(e) => { this.setState({ subject: e.target.value }) }} className='' aria-label="Default select example">
                                    <option value="">Select Subject</option>
                                    <option value="Math">Math</option>
                                    <option value="English">English</option>
                                    <option value="Science">Science</option>
                                </Form.Select>
                            </Form.Group>
                            <Form.Group className="" controlId="formBasicEmail">
                                <Form.Label>Learning Code</Form.Label>
                                <Multiselect
                                    options={this.state.QBMLearningCodeData} // Options to display in the dropdown
                                    selectedValues={this.state.QBMLearningCode} // Preselected value to persist in dropdown
                                    onSelect={this.onSelect} // Function will trigger on select event
                                    onRemove={this.onRemove} // Function will trigger on remove event
                                    displayValue="id" // Property name to display in the dropdown options
                                />
                            </Form.Group>

                            <a onClick={() => { this.setState({ IsModel: false }) }} className="btn btn-theme w-100 mt-4">Save</a>
                        </div>
                    </Modal.Body>

                </Modal>

            </div>
        )
    }
}
