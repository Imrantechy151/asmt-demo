import React, { Component } from 'react'
import { Button, Col, Form, Image, Row } from 'react-bootstrap'
import Url from '../../../helper/url'

export default class list extends Component {
    constructor(props) {
        super(props)
        this.state = {

        }
    }
    render() {
        const { data } = this.props

        const CommonData = ({type, title, std, subject }) => {
            return (
                <>
                    <div className='d-md-flex align-items-center justify-content-between'>
                        <div style={{ flexBasis: '80%' }}>
                            <h6>
                                <span>&#x25B7;</span> 
                                {type=="D"?"Determine the main idea of a passage":null}
                                {type=="C"?"Place value models - tens and ones":title  }

                            </h6>
                        </div>
                        <p ><small className='badge bg-secondary'>{std} </small>   <small className='badge bg-secondary'>{subject}</small></p>
                    </div>
                    {/* <h6>
                        <span>&#x25B7;</span> {title}
                        <p className='mb-1'><b>Standard:</b> {std}  <small><b>Subject :</b> {subject}</small></p>

                    </h6> */}
                </>
            )
        }
        return (
            <div>

                {data.map((item) => {
                    if (item.QBMType == "A") {
                        return (
                            <>
                                <div className='card shadow-sm p-md-4 p-3 border-0 questions-card'>
                                    {/* <h6>
                                        <span>&#x25B7;</span> {item.QBMDetail.QBMDQuestionTitle}
                                        <p className='mb-1'><b>Standard:</b> {item.QBMStd}  <small><b>Subject :</b> {item.QBMSubject}</small></p>
                                       
                                    </h6> */}
                                    {<CommonData title={item.QBMDetail.QBMDQuestionTitle} std={item.QBMStd} subject={item.QBMSubject} />}

                                    <div className='d-flex align-items-center mb-3'>
                                        <Form.Control type="text" placeholder="" className='txtbox' name="ans" /> {item.QBMDetail.QBMDWordAfterBlank}
                                    </div>

                                </div>
                            </>
                        )
                    }
                    else if (item.QBMType == "B") {
                        return (
                            <div className='card shadow-sm p-md-4 p-3 border-0 questions-card'>
                                {/* <h6>
                                    <span>&#x25B7;</span> {item.QBMDetail.QBMDQuestionTitle}
                                </h6> */}
                                {<CommonData title={item.QBMDetail.QBMDQuestionTitle} std={item.QBMStd} subject={item.QBMSubject} />}
                                <Row>
                                    {item.QBMDetail.QBMDDirection == "Vertical"
                                        ?
                                        <Col md={12}>
                                            <div>
                                                <div className='mb-4'>
                                                    <Image src="/images/marketing.png" className='img-fluid' width={25} />&nbsp; <span style={{ fontWeight: '500' }}>{item.QBMDetail.QBMDOperator=="-"?"Subtract":item.QBMDetail.QBMDOperator=="+"?
                                                    "Addition":item.QBMDetail.QBMDOperator=="*"?"Multiplication":item.QBMDetail.QBMDOperator=="/"?"Division":""} :</span>
                                                </div>
                                                <div className='' style={{ width: 80 }}>
                                                    <div className='position-relative'>
                                                        <p className='mb-1 text-end' style={{ fontWeight: '500' }}>{item.QBMDetail.QBMDOperand1}</p>
                                                        <span className='arithmatic-operator'>{item.QBMDetail.QBMDOperator}</span><p className='mb-1 text-end' style={{ fontWeight: '500' }}>{item.QBMDetail.QBMDOperand2}</p>
                                                    </div>
                                                    <hr style={{ backgroundColor: '#000', opacity: 1 }} className='mb-2 mt-2' />
                                                    <Form.Control type="text" placeholder="" className='form-control form-control-sm mb-3 text-end' />

                                                </div>
                                            </div>
                                        </Col>
                                        :
                                        <Col md={12}>
                                            <div>
                                                <div className='mb-4'>
                                                <Image src="/images/marketing.png" className='img-fluid' width={25} />&nbsp; <span style={{ fontWeight: '500' }}>{item.QBMDetail.QBMDOperator=="-"?"Subtract":item.QBMDetail.QBMDOperator=="+"?
                                                    "Addition":item.QBMDetail.QBMDOperator=="*"?"Multiplication":item.QBMDetail.QBMDOperator=="/"?"Division":""} :</span>
                                                </div>
                                                <div className='' style={{}}>
                                                    <div className='position-relative mb-4'>
                                                        <p className='mb-4' style={{ fontWeight: '500', fontSize: 20 }}>{item.QBMDetail.QBMDOperand1} <span>{item.QBMDetail.QBMDOperator}</span> {item.QBMDetail.QBMDOperand2} = <input type="text" placeholder="" className='txtbox border rounded' /></p>
                                                    </div>


                                                </div>
                                            </div>
                                        </Col>
                                    }
                                </Row>
                            </div>
                        )
                    }
                    else if (item.QBMType == "C") {
                        return (
                            <div className='card shadow-sm p-md-4 p-3 border-0 questions-card'>
                                {/* <h6>
                                    <span>&#x25B7;</span> Place value models - tens and ones
                                </h6> */}
                                {<CommonData type={item.QBMType} title={item.QBMDetail.QBMDQuestionTitle} std={item.QBMStd} subject={item.QBMSubject} />}
                                <Row>
                                    <Col md={6}>
                                        <div>
                                            <div className='mb-4'>
                                                <Image src="/images/marketing.png" className='img-fluid' width={25} />&nbsp; <span style={{ fontWeight: '500' }}>{item.QBMDetail.QBMDQuestionTitle}</span>
                                            </div>
                                            <div className='d-flex mb-4'>
                                                <div>
                                                    <div className='overlay-checkbox'>
                                                        <form class="form cf" action="/" method="post">
                                                            <section class="plan cf">
                                                                {/* http://localhost:4000/uploads/question/bank/ */}
                                                                {item.QBMDetail.QBMDOptions.map((img, key) => {
                                                                    return (
                                                                        <div className='image-question'>
                                                                            <input type="radio" name="img" id={img} value={"Option" + key} />
                                                                            <label class="free-label four col" for={img}>
                                                                                <Image src={Url.FileUrl() + img}  className="img-fluid"/>
                                                                            </label>
                                                                        </div>
                                                                    )
                                                                })}                                                                
                                                            </section>

                                                        </form>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </Col>

                                </Row>
                            </div>
                        );
                    }
                    else if (item.QBMType == "D") {
                        return (
                            <div className='card shadow-sm p-md-4 p-3 border-0 questions-card'>
                                {/* <h6>
                                    <span>&#x25B7;</span>Determine the main idea of a passage
                                </h6> */}
                                {<CommonData type={item.QBMType} title={item.QBMDetail.QBMDQuestionTitle} std={item.QBMStd} subject={item.QBMSubject} />}
                                <span className='text-success'>Read the passage</span><br />
                                <div className='bg-light p-md-3 p-2 mb-3'>
                                    <Row>
                                        <Col md={6}>
                                            <div>
                                                <h6 className='text-center mb-1'>{item.QBMDetail.QBMDQuestionTitle}</h6>
                                                {/* <p className='mb-0' about=''>
                                                Lorem ipsum dolor, sit amet consectetur adipisicing elit. Distinctio, ducimus repellat 
                                                similique at autem repudiandae, sequi accusantium accusamus laborum vitae earum corrupti 
                                                aspernatur expedita, recusandae reprehenderit? Amet provident corrupti magni.recusandae
                                                reprehenderit? Amet provident corrupti magni. aspernatur expedita, recusandae reprehenderit? Amet provident corrupti magni.recusandae
                                                reprehenderit? Amet provident corrupti magni.
                                            </p> */}
                                                <p className='mb-0' dangerouslySetInnerHTML={{ __html: item.QBMDetail.QBMDPassage }}></p>
                                            </div>
                                        </Col>
                                        <Col md={6}>
                                            <div>
                                                
                                                <Image src={Url.FileUrl() + item.QBMDetail.QBMDPassageImage}  className='img-fluid' />
                                            </div>
                                        </Col>
                                    </Row>
                                </div>
                                <span className='text-success'>{item.QBMDetail.QBMDOptionTitle}</span><br />
                                <form class="form cf" action="/" method="post">
                                    <section class="plan cf d-block">
                                        {item.QBMDetail.QBMDOptions.map((o, key) => {
                                            return (
                                                <div className={key == 0 ? "mb-3" : ""}>
                                                    <input type="radio" name="img" id={"op" + o} value={"Option" + key} />
                                                    <label class="free-label border-radio col" for={"op" + o}>
                                                        {o}
                                                    </label>
                                                </div>
                                            )
                                        })}
                                        {/* <div>
                                        <input type="radio" name="img" id="img4" value="img4" />
                                        <label class="free-label border-radio col" for="img4">
                                            People who exercise regularly are happier.
                                        </label>
                                    </div> */}
                                    </section>
                                </form>


                            </div>
                        )
                    }
                    else {
                        return null
                    }
                })}
            </div>
        )
    }
}
