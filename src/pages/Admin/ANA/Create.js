import React, { Component } from 'react'
import { Form, Button, Image, Col, Row, Table, Dropdown, Container } from 'react-bootstrap';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import ApiService from '../../../services/ApiServices';
import { ThreeDots } from 'react-loader-spinner';
import Data from '../../../helper/data.json';
import List from './List';
import AlertMessage from '../../../helper/alertMessage';
import { Link } from 'react-router-dom'
import Multiselect from 'multiselect-react-dropdown';

export default class Create extends Component {
    constructor(props) {
        super(props)
        this.state = {
            name: null,
            std: null,
            classLvl: null,
            date: new Date(),
            subject: null,
            Duration: null,
            type: null,
            quiz: [],
            isLoading: false,
            isSubmit: false,
        }
    }
    // async componentDidMount() {
    //     await ApiService.GetQuestionBank().then(res => {
    //         console.log(res.data);
    //         let result = res.data;
    //         if (result.status == 200) {
    //             this.setState({ quiz: result.response, isLoading: false })
    //         }
    //     }, error => {
    //         console.log(error);
    //     })
    // }
    
    Submit = async (e) => {
        e.preventDefault();
        const { name, std, classLvl, date, subject, Duration } = this.state;
        let body = {
            EMName: name, EMStd: std, EMClass: classLvl, EMDate: date, EMSubject: subject, EMDuration: Duration
        }
        console.log(body)
        if (date == null || subject == "" || Duration == "" || std == "") {
            AlertMessage.Error("All Field are Required .")
            return false;
        }

        try {
            this.setState({ isSubmit: true })
            await ApiService.CreateExam(body).then((res) => {
                let data = res.data;
                console.log(data)
                if (data.status == 200) {

                    console.log(data.response);
                    window.localStorage.setItem("exmId", data.response._id)
                    window.location.href = "/admin/ana/Details/" + data.response._id;
                }
                else {

                }
                this.setState({ isSubmit: false });
            }, error => {
                console.log(error);
            })
        }
        catch (error) {
            console.log(error)
        }
    }
    render() {
        const { quiz, isLoading } = this.state;
        if (isLoading) {
            return (
                <div style={{ position: "fixed", left: "50%", top: "30%", bottom: 0, right: "50%", width: "100%", zIndex: 9999 }}>
                    <ThreeDots
                        height="100"
                        width="100"
                        color='#EE4491'
                        ariaLabel='loading'
                    />
                </div>
            )
        }
        return (
            <div>
                <ToastContainer />
                <div>
                    <div className='text-end mb-3 d-md-flex justify-content-between align-items-center'>
                        <h5 className='mb-0 text-theme'>Create ANA</h5>
                    </div>
                    <div>
                        <div className='card  p-3 shadow-sm border-0 mb-3'>
                            <Container fluid>
                                <Row className='align-items-end'>
                                    <Col md={12} >
                                        <Form.Group className='mb-2'>
                                            <Form.Label>ANA Name</Form.Label>
                                            <Form.Control type="text" placeholder='Name' onKeyUp={(e) => { this.setState({ name: e.target.value }) }} />
                                        </Form.Group>
                                    </Col>

                                    <Col md={3}>
                                        <Form.Group controlId="formBasicEmail">
                                            <Form.Label>Year Level</Form.Label>
                                            <Form.Select onChange={(e) => { this.setState({ std: e.target.value }) }} className='' aria-label="Default select example">
                                                <option value="">Select Year Level</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                            </Form.Select>
                                        </Form.Group>
                                    </Col>
                                    <Col md={2} >
                                        <Form.Group >
                                            <Form.Label>Class</Form.Label>
                                            <Form.Select onChange={(e) => { this.setState({ classLvl: e.target.value }) }} className='' aria-label="Default select example">
                                                <option value="">Select Class</option>
                                                <option value="A">A</option>
                                                <option value="B">B</option>
                                                <option value="C">C</option>
                                                <option value="D">D</option>
                                            </Form.Select>
                                        </Form.Group>
                                    </Col>
                                    {/* <Col md={2}>
                                        <Form.Group controlId="formBasicEmail">
                                            <Form.Label>Select Standard</Form.Label>
                                            <Form.Select onChange={(e) => { this.setState({ student: e.target.value }) }} className='' aria-label="Default select example">
                                                <option value="">Student</option>
                                                {Data.Users.map((usr, key) => {
                                                    return (
                                                        < option value={usr.UserId} > {usr.UserId + " - " + usr.Name}</option>
                                                    )
                                                }
                                                )
                                                }
                                            </Form.Select>
                                        </Form.Group>
                                    </Col> */}
                                    <Col md={3}>
                                        <Form.Group controlId="formBasicEmail">
                                            <Form.Label>Subject</Form.Label>
                                            <Form.Select onChange={(e) => { this.setState({ subject: e.target.value }) }} className='' aria-label="Default select example">
                                                <option value="">Select Subject</option>
                                                <option value="Math">Math</option>
                                                <option value="English">English</option>
                                                <option value="Science">Science</option>
                                            </Form.Select>
                                        </Form.Group>
                                    </Col>
                                    <Col md={2}>
                                        <Form.Group controlId="formBasicEmail ">
                                            <Form.Label>Duration</Form.Label>
                                            <Form.Select onChange={(e) => { this.setState({ Duration: e.target.value }) }} className='' aria-label="Default select example">
                                                <option value="">Select Duration</option>
                                                <option value="5 Minutes">5 Minutes</option>
                                                <option value="10 Minutes">10 Minutes</option>
                                                <option value="15 Minutes">15 Minutes</option>
                                                <option value="20 Minutes">20 Minutes</option>
                                                <option value="25 Minutes">25 Minutes</option>
                                                <option value="30 Minutes">30 Minutes</option>
                                                <option value="35 Minutes">35 Minutes</option>
                                                <option value="40 Minutes">40 Minutes</option>
                                                <option value="45 Minutes">45 Minutes</option>
                                                <option value="50 Minutes">50 Minutes</option>
                                                <option value="55 Minutes">55 Minutes</option>
                                                <option value="60 Minutes">60 Minutes</option>
                                            </Form.Select>
                                        </Form.Group>
                                    </Col>
                                    <Col md={2}>
                                        <Button className='btn btn-theme w-100 border-0 px-4 py-2' type="button" onClick={(e) => { this.Submit(e) }}>
                                            {this.state.isSubmit ? (<ThreeDots
                                                height="25"
                                                width="25"
                                                color='#fff'
                                                ariaLabel='loading'
                                            />) : "Save & Next"}
                                        </Button>
                                    </Col>


                                </Row>
                            </Container>
                        </div>
                    </div>

                </div>
                {/* <div className='m-0 card p-md-4 p-3 shadow-sm border-0 mb-3'>
                        <h5 className='mb-0 text-theme'>Choose Question</h5>
                    </div>
                <div className='card-height p-md-4 p-3'>
                    <List data={quiz} />
                </div>
                <div>
                    <Button className='btn btn-theme rounded-pill border-0 mt-3 px-4 py-2' type="button" onClick={(e) => { this.Submit(e) }}>
                        {this.state.isSubmit ? (<ThreeDots
                            height="25"
                            width="25"
                            color='#fff'
                            ariaLabel='loading'
                        />) : "Submit"}
                    </Button>
                </div> */}
            </div >
        )
    }
}

