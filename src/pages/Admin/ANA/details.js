import React, { Component } from 'react'
import { Form, Button, Image, Col, Row, Table, Dropdown, Container, Modal } from 'react-bootstrap';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import ApiService from '../../../services/ApiServices';
import { ThreeDots } from 'react-loader-spinner';
import List from './List';
import Data from '../../../helper/data.json';
import { Quiz } from 'react-quizzes/lib/App';
import Url from '../../../helper/url';
import { Link } from 'react-router-dom'
import Types from '../../../helper/type.json'

export default class details extends Component {
    constructor(props) {
        super(props)
        this.state = {
            type: null, subject: null,
            date: null,
            std: null,
            Duration: null,
            quiz: [],
            isLoading: true,
            isSubmit: false,
            count: 0,
            details: null,
            IsModel: false,
            EMaster: [],Checked1:"1",Checked2:"2",
            activeClass:""
        }
    }
    
    async componentDidMount() {
        //   console.log(Params)

        var id = window.localStorage.getItem("exmId");
        console.log(id)
        await ApiService.GetExamDetails(id).then(res => {
            console.log(res.data);
            let result = res.data;
            if (result.status == 200) {
                this.setState({ details: result.response.EMaster, EMaster: result.response.EDMaster })
                console.log(result.response)
                let datas = {
                    QBMStd: result.response.EMStd,

                }
                ApiService.GetQuestionBank(datas).then(res => {
                    //console.log(res.data);
                    let result = res.data;
                    if (result.status == 200) {
                        this.setState({ quiz: result.response, isLoading: false })
                    }
                }, error => {
                    console.log(error);
                })
            }
        }, error => {
            console.log(error);
        })


        window.addEventListener('scroll', () => {
            let activeClass = '';
           
            if(window.scrollY === 50){
                activeClass = 'sticky-header';
            }
            else{
                activeClass = '';
            }
            this.setState({ activeClass });
         });

    }

     changeScroll=()=>{
        const scrollValue = document.documentElement.scrollTop;
        if (scrollValue > 100){
            this.state.activeClass(true);
        }else{
            this.state.activeClass(false);
        }
     }

    render() {
        const { quiz, isLoading, details, count, IsModel } = this.state;
        if (isLoading) {
            return (
                <div style={{ position: "fixed", left: "50%", top: "30%", bottom: 0, right: "50%", width: "100%", zIndex: 9999 }}>
                    <ThreeDots
                        height="100"
                        width="100"
                        color='#EE4491'
                        ariaLabel='loading'
                    />
                </div>
            )
        }
        let flag = 1;
        let name = Data.Users.find(a => a.UserId == details.EMStudent);
        const CommonData = ({ type, title, std, subject }) => {
            return (
                <>

                    <div className='' style={{ width: '90%' }}>
                        <h6>
                            {type == "D" ? "Determine the main idea of a passage" : 
                            type == "C" ? "Place value models - tens and ones" : title}
                        </h6>
                    </div>

                    {/* <h6>
                    <span>&#x25B7;</span> {title}
                    <p className='mb-1'><b>Standard:</b> {std}  <small><b>Subject :</b> {subject}</small></p>

                </h6> */}
                </>
            )
        }

        const CommonData1 = ({ type, std, subject, code, flags }) => {
            var tp = Types.find(a => a.value == type);
            console.log("flag :" + flags);
            return (
                <div className='card-header d-md-flex justify-content-between bg-white p-3' style={{ borderBottomColor: '#f0f0f0' }}>
                    <div>
                        <small class="me-2 text-secondary ">Created by <span style={{ fontWeight: '600' }}>{flags == 1 ? "Best Performance" : "John Smith"}</span></small>
                        <span className='badge btn-theme fw-normal me-2' data-bs-toggle="tooltip" title='Question Type'>{tp ? tp.name : ""}</span>
                    </div>
                    <div>
                        <span className='badge bg-color1 text-dark fw-normal me-2' title='Year Level'> {std}</span>
                        <span className='badge bg-color1 text-dark me-2 fw-normal' title="Subject"> {subject}</span>
                        <span className='badge bg-color1 text-dark me-2 fw-normal' title='LeaningCode'>ACEAD ACELY1713</span>
                        <span className='badge bg-color1 text-dark fw-normal me-2' title='Toughness' data-bs-toggle="tooltip">Easy</span>

                        {/* <span className='badge bg-color1 text-dark fw-normal me-2' title='Question Code'>{code}</span> */}
                    </div>
                </div>
            )
        }

        const CommonData2 = ({ type, title, std, subject, timer, explanation }) => {
            return (
                <div className='card-footer px-4 py-2 d-md-flex align-items-center justify-content-between p-3 border-0' style={{ backgroundColor: '#f7fcfc' }}>
                    <div>
                        <span className='btn-link text-primary text-decoration-none small' onClick={() => { this.setState({ IsModel: true, explanation: explanation }) }} style={{ color: 'gray', cursor: 'pointer' }}>View Explanation  </span> &nbsp;
                    </div>
                </div>
            )
        }
        return (
            <div>
                <Modal size='lg' show={IsModel} onHide={() => { this.setState({ IsModel: false }) }}>
                    <Modal.Header closeButton>
                        <Modal.Title>Explanation</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div dangerouslySetInnerHTML={{ __html: this.state.explanation }}></div>
                    </Modal.Body>

                </Modal>

                <div className="card p-md-3 p-3 shadow-sm border-0 mb-2 sticky-header">
                <div className='text-end mb-2 pb-3 d-md-flex justify-content-between align-items-center bdr-btm'>
                    <h5 className='mb-0 text-theme'>ANA Details</h5>
                    <div>
                        <span className="text-secondary"><span className="text-success">[3]</span> Questions Selected</span>
                        <Link to='/admin/ana/' className='btn btn-theme btn-sm ms-3'>Save the ANA</Link>
                    </div>                    
                </div>
                <div className='card mb-3 border-0 py-2'>
                    <div className='d-md-flex justify-content-between align-items-center'>
                        <div>
                            <p className='text-theme mb-2' style={{ fontWeight: '600', fontSize: 18 }}> {details.EMName}</p>                            
                        </div>
                        <div className='' style={{ width: '45%' }}>
                            <div className='d-flex align-items-center'>
                                <div className='bg-color6 text-center me-2 rounded py-2' style={{ flexBasis: 180 }}>
                                    <span className='fw-normal text-start' style={{ fontSize: 12 }}>Year Level</span>
                                    <p className='mb-0' style={{ fontSize: 13, fontWeight: '700' }}>{details.EMStd}</p>
                                </div>
                                <div className='bg-color4 text-center me-2 rounded py-2' style={{ flexBasis: 180 }}>
                                    <span className='fw-normal text-start' style={{ fontSize: 12 }}>Class</span>
                                    <p className='mb-0' style={{ fontSize: 13, fontWeight: '700' }}>{details.EMStd}{details.EMClass}</p>
                                </div>

                                <div className='bg-color2 text-center me-2 rounded py-2' style={{ flexBasis: 180 }}>
                                    <span className='fw-normal text-start' style={{ fontSize: 12 }}>Subject</span>
                                    <p className='mb-0' style={{ fontSize: 13, fontWeight: '600' }}>{details.EMSubject}</p>
                                </div>

                                <div className='bg-color3 text-center me-2 rounded py-2' style={{ flexBasis: 180 }}>
                                    <span className='fw-normal text-start' style={{ fontSize: 12 }}>Duration</span>
                                    <p className='mb-0' style={{ fontSize: 13, fontWeight: '600' }}>{details.EMDuration}</p>
                                </div>
                                <div className='bg-color1 text-center rounded py-2' style={{ flexBasis: 180 }}>
                                    <span className='fw-normal text-start' style={{ fontSize: 12 }}>Questions</span>
                                    <p className='mb-0' style={{ fontSize: 13, fontWeight: '600' }}>{details.EMQuestionCount}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <span className="text-secondary" style={{ fontWeight: '600',}}>Choose Questions</span>
                <div className='d-flex justify-content-between align-items-center'>                       
                        <div className="text-secondary">                            
                            <input type="checkbox"  className="qlist" value="1" onChange={
                                (e)=>{
                                    if(this.state.Checked1=="1") 
                                    {
                                        this.setState({Checked1:"3"})
                                    }
                                    else{
                                        this.setState({Checked1:"1"})
                                    }
                          }} checked={this.state.Checked1=="1"?true:false}  /><span style={{fontSize:14}}> BP Question List (48)</span> &nbsp;
                            <input type="checkbox"  className="qlist" value="2" onChange={
                                (e)=>{
                                    if(this.state.Checked2=="2") 
                                    {
                                        this.setState({Checked2:"3"})
                                    }
                                    else{
                                        this.setState({Checked2:"2"})
                                    }
                          }} checked={this.state.Checked2=="2"?true:false} /><span style={{fontSize:14}}> My Question List (52)</span>
                        </div>
                        <div className='d-flex align-items-center'>
                        <div className='d-flex align-items-center'>
                            <select className='form-control form-control-sm select-type test-details-filter border me-2' style={{ height: 35,}}>
                                <option value="">Select Toughness Level</option>
                                <option value="A">Easy</option>
                                <option value="B">Medium</option>
                                <option value="C">Hard</option>
                            </select>
                            <select className='form-control form-control-sm select-type test-details-filter border me-2' style={{ height: 35,}}>
                                <option value="">Select Learning Code</option>
                                <option value="A">ACEAD ACELA1463</option>
                                <option value="B">ACEAD ACELY1660</option>
                                <option value="C">ACEAD ACELY1713</option>
                            </select>
                            <select className='form-control form-control-sm select-type test-details-filter border me-2' style={{ height: 35, }}>
                                <option value="">Select Question Type</option>
                                {Types.map((item) => (
                                    <option value={item.value}>{item.name}</option>
                                ))}                                
                            </select>                            
                        </div>
                        <div>
                            <a href="#" className="btn btn-sm btn-theme d-flex align-items-center" style={{height:35}}>Show Questions</a>
                        </div>
                        </div>
                        
                    </div>
                </div>

                <div className='card p-md-4 p-3 shadow-sm border-0 mb-2'>
                        <div>
                            <label>Exam instruction</label>
                            <input type="text" name="" className='form-control form-control-sm' />
                        </div>
                    </div>
                <div>                    
                    {quiz.map((item) => {
                        if (flag == 1) {
                            flag = 0;
                        }
                        else {
                            flag = 1;
                        }
                        console.log(flag);
                        if (item.QBMType == "A") {
                            return (
                                <>
                                    <div className="select-qoestion test_details">
                                        <input id={"q" + item._id} name="option1" onChange={(e) => { this.setState({ std: e.target.value }) }} type="checkbox" className='chbx' />
                                        <label for={"q" + item._id} className="card shadow-sm p-0 border-0 questions-card lbl">
                                            {<CommonData1 type={item.QBMType} std={item.QBMStd} subject={item.QBMSubject} code={item._id} flags={flag} />}
                                            <div className='card-body p-md-4 p-3 '>
                                                {<CommonData title={item.QBMDetail.QBMDQuestionTitle} std={item.QBMStd} subject={item.QBMSubject} />}
                                                <div className='d-flex align-items-center mb-3'>
                                                    <Form.Control type="text" placeholder="" className='txtbox' name="ans" /> {item.QBMDetail.QBMDWordAfterBlank}
                                                </div>
                                            </div>
                                            {<CommonData2 explanation={item.QBMExplanation} timer={item.QBMTimerValue} />}
                                        </label>
                                    </div>
                                </>
                            )
                        }
                        else if (item.QBMType == "B" || item.QBMType == "BA" || item.QBMType == "BB" || item.QBMType == "BC") {
                            return (

                                <div className="select-qoestion test_details">
                                    <input id={"q" + item._id} name="option1" onChange={(e) => { this.setState({ std: e.target.value }) }} type="checkbox" className='chbx' />

                                    <label for={"q" + item._id} className='card shadow-sm p-0 border-0 questions-card lbl'>
                                        {<CommonData1 type={item.QBMType} std={item.QBMStd} subject={item.QBMSubject} code={item._id} flags={flag} />}
                                        <div className='card-body p-md-4 p-3 '>
                                            {<CommonData title={item.QBMDetail.QBMDQuestionTitle} std={item.QBMStd} subject={item.QBMSubject} />}
                                            <Row>
                                                {item.QBMDetail.QBMDDirection == "Vertical"
                                                    ?
                                                    <Col md={12}>
                                                        <div>
                                                            <div className='mb-4'>
                                                                <span style={{ fontWeight: '500' }}>{item.QBMDetail.QBMDOperator == "-" ? "Subtract" : item.QBMDetail.QBMDOperator == "+" ?
                                                                    "Addition" : item.QBMDetail.QBMDOperator == "x" ? "Multiplication" : item.QBMDetail.QBMDOperator == "/" ? "Division" : ""} :</span>
                                                            </div>
                                                            <div className='' style={{ width: 80 }}>
                                                                <div className='position-relative'>
                                                                    <p className='mb-1 text-end' style={{ fontWeight: '500' }}>{item.QBMDetail.QBMDOperand1}</p>
                                                                    <span className='arithmatic-operator'>{item.QBMDetail.QBMDOperator}</span><p className='mb-1 text-end' style={{ fontWeight: '500' }}>{item.QBMDetail.QBMDOperand2}</p>
                                                                </div>
                                                                <hr style={{ backgroundColor: '#000', opacity: 1 }} className='mb-2 mt-2' />
                                                                <Form.Control type="text" placeholder="" className='form-control form-control-sm mb-3 text-end' />

                                                            </div>
                                                        </div>
                                                    </Col>
                                                    :
                                                    <Col md={12}>
                                                        <div>
                                                            <div className='mb-4'>
                                                                <span style={{ fontWeight: '500' }}>{item.QBMDetail.QBMDOperator == "-" ? "Subtract" : item.QBMDetail.QBMDOperator == "+" ?
                                                                    "Addition" : item.QBMDetail.QBMDOperator == "x" ? "Multiplication" : item.QBMDetail.QBMDOperator == "/" ? "Division" : ""} :</span>
                                                            </div>
                                                            <div className='' style={{}}>
                                                                <div className='position-relative mb-4'>
                                                                    <p className='mb-4' style={{ fontWeight: '500', fontSize: 20 }}>{item.QBMDetail.QBMDOperand1} <span>{item.QBMDetail.QBMDOperator}</span> {item.QBMDetail.QBMDOperand2} = <input type="text" placeholder="" className='txtbox border rounded' /></p>
                                                                </div>


                                                            </div>
                                                        </div>
                                                    </Col>
                                                }
                                            </Row>

                                        </div>
                                        {<CommonData2 explanation={item.QBMExplanation} timer={item.QBMTimerValue} />}
                                    </label>
                                </div>
                            )
                        }
                        else if (item.QBMType == "C") {
                            return (
                                <div className="select-qoestion test_details">
                                    <input id={"q" + item._id} name="option1" onChange={(e) => { this.setState({ std: e.target.value }) }} type="checkbox" className='chbx' />

                                    <label for={"q" + item._id} className='card shadow-sm p-0 border-0 questions-card lbl'>
                                        {<CommonData1 type={item.QBMType} std={item.QBMStd} subject={item.QBMSubject} code={item._id} flags={flag} />}
                                        <div className='card-body p-md-4 p-3 '>
                                            {<CommonData type={item.QBMType} title={item.QBMDetail.QBMDQuestionTitle} std={item.QBMStd} subject={item.QBMSubject} />}
                                            <Row>
                                                <Col md={6}>
                                                    <div>
                                                        <div className='mb-4'>
                                                            <span style={{ fontWeight: '500' }}>{item.QBMDetail.QBMDQuestionTitle}</span>
                                                        </div>
                                                        <div className='d-flex mb-4'>
                                                            <div>
                                                                <div className='overlay-checkbox'>
                                                                    <form class="form cf" action="/" method="post">
                                                                        <section class="plan cf">
                                                                            {/* http://localhost:4000/uploads/question/bank/ */}
                                                                            {item.QBMDetail.QBMDOptions.map((img, key) => {
                                                                                return (
                                                                                    <div className='image-question'>
                                                                                        <input type="radio" name="img" id={img} value={"Option" + key} />
                                                                                        <label class="free-label four col" for={img}>
                                                                                            <Image src={Url.FileUrl() + img} className="img-fluid" />
                                                                                        </label>
                                                                                    </div>
                                                                                )
                                                                            })}
                                                                            {/* <div className='image-question'>
                                                                    <input type="radio" name="img" id="img1" value="img1" />
                                                                    <label class="free-label four col" for="img1">
                                                                        <Image src='/images/bar.png' />

                                                                    </label>
                                                                </div>
                                                                <div className='image-question'>
                                                                    <input type="radio" name="img" id="img2" value="img2" />
                                                                    <label class="free-label four col" for="img2">
                                                                        <Image src='/images/bar2.png' />
                                                                    </label>
                                                                </div> */}
                                                                        </section>

                                                                    </form>
                                                                </div>
                                                            </div>

                                                        </div>

                                                    </div>
                                                </Col>

                                            </Row>
                                        </div>
                                        {<CommonData2 explanation={item.QBMExplanation} timer={item.QBMTimerValue} />}

                                    </label>
                                </div>
                            );
                        }
                        else if (item.QBMType == "D") {
                            return (

                                <div className="select-qoestion test_details">
                                    <input id={"q" + item._id} name="option1" onChange={(e) => { this.setState({ std: e.target.value }) }} type="checkbox" className='chbx' />

                                    <label for={"q" + item._id} className='card shadow-sm p-0 border-0 questions-card lbl'>
                                        {<CommonData1 type={item.QBMType} std={item.QBMStd} subject={item.QBMSubject} code={item._id} flags={flag} />}
                                        <div className='card-body p-md-4 p-3 '>

                                            {<CommonData type={item.QBMType} title={item.QBMDetail.QBMDQuestionTitle} std={item.QBMStd} subject={item.QBMSubject} />}
                                            <span className='text-success'>Read the passage</span><br />
                                            <div className='bg-light p-md-3 p-2 mb-3'>
                                                <Row>
                                                    <Col md={6}>
                                                        <div>
                                                            <h6 className='text-center mb-1'>{item.QBMDetail.QBMDQuestionTitle}</h6>

                                                            <p className='mb-0' dangerouslySetInnerHTML={{ __html: item.QBMDetail.QBMDPassage }}></p>
                                                        </div>
                                                    </Col>
                                                    <Col md={6}>
                                                        <div>

                                                            <Image src={Url.FileUrl() + item.QBMDetail.QBMDPassageImage} className='img-fluid' style={{ height: 250, objectFit: "cover" }} />
                                                        </div>
                                                    </Col>
                                                </Row>
                                            </div>
                                            <span className='text-success'>{item.QBMDetail.QBMDOptionTitle}</span><br />
                                            <form class="form cf" action="/" method="post">
                                                <section class="plan cf d-block">
                                                    {item.QBMDetail.QBMDOptions.map((o, key) => {
                                                        return (
                                                            <div className={"mb-2"}>
                                                                <input type="radio" name="img" id={"op" + o} value={"Option" + key} />
                                                                <label class="free-label border-radio col" for={"op" + o}>
                                                                    {o}
                                                                </label>
                                                            </div>
                                                        )
                                                    })}
                                                    {/* <div>
                                        <input type="radio" name="img" id="img4" value="img4" />
                                        <label class="free-label border-radio col" for="img4">
                                            People who exercise regularly are happier.
                                        </label>
                                    </div> */}
                                                </section>
                                            </form>

                                        </div>
                                        {<CommonData2 explanation={item.QBMExplanation} timer={item.QBMTimerValue} />}


                                    </label>
                                </div>
                            )
                        }
                        else {
                            return null
                        }
                    })}
                </div>

                <Link to='/admin/ana/' className='btn btn-theme '>Save the ANA</Link>
            </div>
        )
    }
}
