import React, { Component } from 'react';
import { Accordion, Table } from 'react-bootstrap';

export default class analytics extends Component {
    render() {
        return (
            <>
                <div className='card p-3 shadow-sm border-0 mb-2'>
                    <div className='text-end d-md-flex justify-content-between'>
                        <h5 className='mb-0'>Analytics</h5>
                    </div>
                </div>
                <div className=''>
                    <Accordion defaultActiveKey="0">
                        <Accordion.Item eventKey="0" className='card shadow-sm border-0 mb-2 rounded'>
                            <Accordion.Header>1. Question type wise report</Accordion.Header>
                            <Accordion.Body>
                                <Table bordered className='analytics-table mb-0 w-50'>
                                    <thead>
                                        <tr>
                                            <th rowSpan="2" className='table-light'>Questions Type</th>
                                            <th colSpan="3" className='table-success'>Questions Count</th>
                                        </tr>
                                        <tr className='bg-light'>
                                            <th>Total <small>(2800)</small></th>
                                            <th>Admin <small>(1750)</small></th>
                                            <th>Teachers <small>(1250)</small></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td className='table-light text-start'>Fill In The Blanks</td>
                                            <td>50(%)</td>
                                            <td>64(%)</td>
                                            <td>36(%)</td>
                                        </tr>
                                        <tr>
                                            <td className='table-light text-start'>Arithmetic Addition</td>
                                            <td>74(%)</td>
                                            <td>36(%)</td>
                                            <td>64(%)</td>
                                        </tr>
                                        <tr>
                                            <td className='table-light text-start'>Arithmetic Subtraction</td>
                                            <td>65(%)</td>
                                            <td>25(%)</td>
                                            <td>75(%)</td>
                                        </tr>
                                        <tr>
                                            <td className='table-light text-start'>Arithmetic Multiplication</td>
                                            <td>38(%)</td>
                                            <td>42(%)</td>
                                            <td>58(%)</td>
                                        </tr>
                                        <tr>
                                            <td className='table-light text-start'>Arithmetic Division</td>
                                            <td>69(%)</td>
                                            <td>21(%)</td>
                                            <td>79(%)</td>
                                        </tr>
                                        <tr>
                                            <td className='table-light text-start'>Multiple Choice Image</td>
                                            <td>58(%)</td>
                                            <td>72(%)</td>
                                            <td>28(%)</td>
                                        </tr>
                                    </tbody>
                                </Table>
                            </Accordion.Body>
                        </Accordion.Item>

                        <Accordion.Item eventKey="1" className='card shadow-sm border-0 mb-2 rounded'>
                            <Accordion.Header>2. AC code wise questions</Accordion.Header>
                            <Accordion.Body>
                                <Table bordered className='analytics-table mb-0 w-50'>
                                    <thead>
                                        <tr>
                                            <th rowSpan="2" className='table-light'>AC Code</th>
                                            <th colSpan="3" className='table-success'>Questions Count</th>
                                        </tr>
                                        <tr className='bg-light'>
                                            <th>Total <small>(1500)</small></th>
                                            <th>Admin <small>(1000)</small></th>
                                            <th>Teachers <small>(500)</small></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td className='table-light text-start'>ACEAD ACELY1713</td>
                                            <td>50(%)</td>
                                            <td>48(%)</td>
                                            <td>52(%)</td>
                                        </tr>
                                        <tr>
                                            <td className='table-light text-start'>ACEAD ACELY1713</td>
                                            <td>74(%)</td>
                                            <td>87(%)</td>
                                            <td>13(%)</td>
                                        </tr>
                                        <tr>
                                            <td className='table-light text-start'>ACEAD ACELY1713</td>
                                            <td>65(%)</td>
                                            <td>25(%)</td>
                                            <td>75(%)</td>
                                        </tr>
                                    </tbody>
                                </Table>
                            </Accordion.Body>
                        </Accordion.Item>

                        <Accordion.Item eventKey="2" className='card shadow-sm border-0 mb-2 rounded'>
                            <Accordion.Header>3. AC code wise test</Accordion.Header>
                            <Accordion.Body>
                                <Table bordered className='analytics-table mb-0 w-50'>
                                    <thead>
                                        <tr>
                                            <th className='table-light'>AC Code</th>
                                            <th className='table-success'>Test Count</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td className='table-light text-start'>ACEAD ACELY1713</td>
                                            <td>50(%)</td>
                                        </tr>
                                        <tr>
                                            <td className='table-light text-start'>ACEAD ACELY1713</td>
                                            <td>74(%)</td>
                                        </tr>
                                        <tr>
                                            <td className='table-light text-start'>ACEAD ACELY1713</td>
                                            <td>65(%)</td>
                                        </tr>
                                    </tbody>
                                </Table>
                            </Accordion.Body>
                        </Accordion.Item>

                        <Accordion.Item eventKey="4" className='card shadow-sm border-0 mb-2 rounded'>
                            <Accordion.Header>4. Year Level wise questions</Accordion.Header>
                            <Accordion.Body>
                                <Table bordered className='analytics-table mb-0 w-50'>
                                    <thead>
                                        <tr>
                                            <th rowSpan="2" className='table-light'>Year Level</th>
                                            <th colSpan="3" className='table-success'>Questions Count</th>
                                        </tr>
                                        <tr className='bg-light'>
                                            <th>Total <small>(850)</small></th>
                                            <th>Admin <small>(600)</small></th>
                                            <th>Teachers <small>(250)</small></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td className='table-light text-start'>Year 1</td>
                                            <td>50(%)</td>
                                            <td>28(%)</td>
                                            <td>72(%)</td>
                                        </tr>
                                        <tr>
                                            <td className='table-light text-start'>Year 2</td>
                                            <td>74(%)</td>
                                            <td>63(%)</td>
                                            <td>37(%)</td>
                                        </tr>
                                        <tr>
                                            <td className='table-light text-start'>Year 3</td>
                                            <td>65(%)</td>
                                            <td>29(%)</td>
                                            <td>61(%)</td>
                                        </tr>
                                    </tbody>
                                </Table>
                            </Accordion.Body>
                        </Accordion.Item>

                        <Accordion.Item eventKey="5" className='card shadow-sm border-0 mb-2 rounded'>
                            <Accordion.Header>5. Year Level wise Tests</Accordion.Header>
                            <Accordion.Body>
                                <Table bordered className='analytics-table mb-0 w-50'>
                                    <thead>
                                        <tr>
                                            <th className='table-light'>Year Level</th>
                                            <th className='table-success'>Test</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td className='table-light text-start'>Year 1</td>
                                            <td>50 (%)</td>
                                        </tr>
                                        <tr>
                                            <td className='table-light text-start'>Year 2</td>
                                            <td>74 (%)</td>
                                        </tr>
                                        <tr>
                                            <td className='table-light text-start'>Year 3</td>
                                            <td>65 (%)</td>
                                        </tr>
                                    </tbody>
                                </Table>
                            </Accordion.Body>
                        </Accordion.Item>

                        <Accordion.Item eventKey="6" className='card shadow-sm border-0 mb-2 rounded'>
                            <Accordion.Header>6. Subject wise Tests</Accordion.Header>
                            <Accordion.Body>
                                <Table bordered className='analytics-table mb-0 w-50'>
                                    <thead>
                                        <tr>
                                            <th rowSpan="2" className='table-light'>Subject</th>
                                            <th colSpan="3" className='table-success'>Test Count</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td className='table-light text-start'>English</td>
                                            <td>50(%)</td>
                                        </tr>
                                        <tr>
                                            <td className='table-light text-start'>Science</td>
                                            <td>74(%)</td>
                                        </tr>
                                        <tr>
                                            <td className='table-light text-start'>Maths</td>
                                            <td>65(%)</td>
                                        </tr>
                                    </tbody>
                                </Table>
                            </Accordion.Body>
                        </Accordion.Item>

                        <Accordion.Item eventKey="7" className='card shadow-sm border-0 mb-2 rounded'>
                            <Accordion.Header>7. Subject wise Questions</Accordion.Header>
                            <Accordion.Body>
                                <Table bordered className='analytics-table mb-0 w-50'>
                                    <thead>
                                        <tr>
                                            <th rowSpan="2" className='table-light'>Subject</th>
                                            <th colSpan="3" className='table-success'>Questions Count</th>
                                        </tr>
                                        <tr className='bg-light'>
                                            <th>Total <small>(900)</small></th>
                                            <th>Admin <small>(720)</small></th>
                                            <th>Teachers <small>(180)</small></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td className='table-light text-start'>English</td>
                                            <td>50(%)</td>
                                            <td>28(%)</td>
                                            <td>22(%)</td>
                                        </tr>
                                        <tr>
                                            <td className='table-light text-start'>Science</td>
                                            <td>74(%)</td>
                                            <td>13(%)</td>
                                            <td>13(%)</td>
                                        </tr>
                                        <tr>
                                            <td className='table-light text-start'>Maths</td>
                                            <td>65(%)</td>
                                            <td>25(%)</td>
                                            <td>15(%)</td>
                                        </tr>
                                    </tbody>
                                </Table>
                            </Accordion.Body>
                        </Accordion.Item>

                        <Accordion.Item eventKey="8" className='card shadow-sm border-0 mb-2 rounded'>
                            <Accordion.Header>8. Rating wise Questions</Accordion.Header>
                            <Accordion.Body>
                                <Table bordered className='analytics-table mb-0 w-50'>
                                    <thead>
                                        <tr>
                                            <th className='table-light'>Teachers Ratings</th>
                                            <th className='table-success'>Questions Count</th>
                                        </tr>

                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td className='table-light text-start'>Extremely Good(10)</td>
                                            <td>230 (15%)</td>
                                        </tr>
                                        <tr>
                                            <td className='table-light text-start'>Very Good(8)</td>
                                            <td>590 (38%)</td>
                                        </tr>
                                        <tr>
                                            <td className='table-light text-start'>Good(6)</td>
                                            <td>410 (26%)</td>
                                        </tr>
                                        <tr>
                                            <td className='table-light text-start'>Not Bad(5)</td>
                                            <td>210 (13%)</td>
                                        </tr>
                                        <tr>
                                            <td className='table-light text-start'>Bad(3)</td>
                                            <td>80 (5%)</td>
                                        </tr>
                                        <tr>
                                            <td className='table-light text-start'>The Worst(0)</td>
                                            <td>45 (3%)</td>
                                        </tr>
                                    </tbody>
                                </Table>
                            </Accordion.Body>
                        </Accordion.Item>

                        <Accordion.Item eventKey="3" className='card shadow-sm border-0 mb-2 rounded'>
                            <Accordion.Header>9. Questions type wise Rating</Accordion.Header>
                            <Accordion.Body>
                                <Table bordered className='analytics-table mb-0'>
                                    <thead>
                                        <tr>
                                            <th className='table-light'>Questions Type</th>
                                            <th className='table-success'>Total (1250)</th>
                                            <th className='table-success'>Extremely Good(10)</th>
                                            <th className='table-success'>Very Good (8)</th>
                                            <th className='table-success'>Good (6)</th>
                                            <th className='table-success'>Not Bad (5)</th>
                                            <th className='table-success'>Bad (3)</th>
                                            <th className='table-success'>The Worst (0)</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td className='table-light text-start'>Fill in the blank</td>
                                            <td>126</td>
                                            <td>56</td>
                                            <td>25</td>
                                            <td>10</td>
                                            <td>8</td>
                                            <td>20</td>
                                            <td>7</td>
                                        </tr>  
                                        <tr>
                                            <td className='table-light text-start'>Subtraction</td>
                                            <td>540</td>
                                            <td>45</td>
                                            <td>44</td>
                                            <td>15</td>
                                            <td>11</td>
                                            <td>28</td>
                                            <td>47</td>
                                        </tr>  
                                        <tr>
                                            <td className='table-light text-start'>Compare Image</td>
                                            <td>120</td>
                                            <td>23</td>
                                            <td>36</td>
                                            <td>68</td>
                                            <td>52</td>
                                            <td>45</td>
                                            <td>68</td>
                                        </tr>                                      
                                    </tbody>
                                </Table>
                            </Accordion.Body>
                        </Accordion.Item>

                        <Accordion.Item eventKey="9" className='card shadow-sm border-0 mb-2 rounded'>
                            <Accordion.Header>10. Toughness level wise Questions</Accordion.Header>
                            <Accordion.Body>
                                <Table bordered className='analytics-table mb-0 w-50'>
                                    <thead>
                                        <tr>
                                            <th rowSpan="2" className='table-light'>Toughness Level</th>
                                            <th colSpan="3" className='table-success'>Questions Count</th>
                                        </tr>
                                        <tr className='bg-light'>
                                            <th>Total <small>(1000)</small></th>
                                            <th>Admin <small>(620)</small></th>
                                            <th>Teachers <small>(380)</small></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td className='table-light text-start'>Easy</td>
                                            <td>50(%)</td>
                                            <td>28(%)</td>
                                            <td>72(%)</td>
                                        </tr>
                                        <tr>
                                            <td className='table-light text-start'>Medium</td>
                                            <td>74(%)</td>
                                            <td>87(%)</td>
                                            <td>13(%)</td>
                                        </tr>
                                        <tr>
                                            <td className='table-light text-start'>Hard</td>
                                            <td>65(%)</td>
                                            <td>55(%)</td>
                                            <td>45(%)</td>
                                        </tr>
                                    </tbody>
                                </Table>
                            </Accordion.Body>
                        </Accordion.Item>

                        <Accordion.Item eventKey="10" className='card shadow-sm border-0 mb-2 rounded'>
                            <Accordion.Header>11. Question's Matrices (Attendees,right,wrong,In-test count)</Accordion.Header>
                            <Accordion.Body>
                                <Table bordered className='analytics-table mb-0'>
                                    <thead>
                                        <tr>
                                            <th className='table-light'>Questions Code</th>
                                            <th className='table-success'>Questions Type</th>
                                            <th className='table-success'>Questions Name</th>
                                            <th className='table-success'>Attendees <i class="fa fa-long-arrow-down small" style={{ fontSize: 10 }}></i><i class="fa fa-long-arrow-up small" style={{ fontSize: 10 }}></i></th>
                                            <th className='table-success'>Correct <i class="fa fa-long-arrow-down small" style={{ fontSize: 10 }}></i><i class="fa fa-long-arrow-up small" style={{ fontSize: 10 }}></i></th>
                                            <th className='table-success'>Wrong <i class="fa fa-long-arrow-down small" style={{ fontSize: 10 }}></i><i class="fa fa-long-arrow-up small" style={{ fontSize: 10 }}></i></th>
                                            <th className='table-success'>In Test <i class="fa fa-long-arrow-down small" style={{ fontSize: 10 }}></i><i class="fa fa-long-arrow-up small" style={{ fontSize: 10 }}></i></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td className='table-light text-start'>SSDF2356R</td>
                                            <td>Fill in the blank</td>
                                            <td>Place value models - tens and ones</td>
                                            <td>98</td>
                                            <td>91</td>
                                            <td>7</td>
                                            <td>10</td>
                                        </tr>
                                        <tr>
                                            <td className='table-light text-start'>FFDD2563E</td>
                                            <td>Arithmetic Addition</td>
                                            <td>Subtract a one-digit number from a two-digit number - without regrouping</td>
                                            <td>85</td>
                                            <td>75</td>
                                            <td>10</td>
                                            <td>5</td>
                                        </tr>
                                        <tr>
                                            <td className='table-light text-start'>POLK2365O</td>
                                            <td>Arithmetic Division</td>
                                            <td>Determine the main idea of a passage It's Simple! It's Magic!</td>
                                            <td>95</td>
                                            <td>75</td>
                                            <td>25</td>
                                            <td>8</td>
                                        </tr>
                                        <tr>
                                            <td className='table-light text-start'>SSDF2356R</td>
                                            <td>Fill in the blank</td>
                                            <td>Place value models - tens and ones</td>
                                            <td>98</td>
                                            <td>91</td>
                                            <td>7</td>
                                            <td>10</td>
                                        </tr>
                                        <tr>
                                            <td className='table-light text-start'>FFDD2563E</td>
                                            <td>Arithmetic Addition</td>
                                            <td>Subtract a one-digit number from a two-digit number - without regrouping</td>
                                            <td>85</td>
                                            <td>75</td>
                                            <td>10</td>
                                            <td>5</td>
                                        </tr>
                                        <tr>
                                            <td className='table-light text-start'>POLK2365O</td>
                                            <td>Arithmetic Division</td>
                                            <td>Determine the main idea of a passage It's Simple! It's Magic!</td>
                                            <td>95</td>
                                            <td>75</td>
                                            <td>25</td>
                                            <td>8</td>
                                        </tr>
                                        <tr>
                                            <td className='table-light text-start'>SSDF2356R</td>
                                            <td>Fill in the blank</td>
                                            <td>Place value models - tens and ones</td>
                                            <td>98</td>
                                            <td>91</td>
                                            <td>7</td>
                                            <td>10</td>
                                        </tr>
                                        <tr>
                                            <td className='table-light text-start'>FFDD2563E</td>
                                            <td>Arithmetic Addition</td>
                                            <td>Subtract a one-digit number from a two-digit number - without regrouping</td>
                                            <td>85</td>
                                            <td>75</td>
                                            <td>10</td>
                                            <td>5</td>
                                        </tr>
                                        <tr>
                                            <td className='table-light text-start'>POLK2365O</td>
                                            <td>Arithmetic Division</td>
                                            <td>Determine the main idea of a passage It's Simple! It's Magic!</td>
                                            <td>95</td>
                                            <td>75</td>
                                            <td>25</td>
                                            <td>8</td>
                                        </tr>
                                        <tr>
                                            <td className='table-light text-start'>FFDD2563E</td>
                                            <td>Arithmetic Addition</td>
                                            <td>Subtract a one-digit number from a two-digit number - without regrouping</td>
                                            <td>85</td>
                                            <td>75</td>
                                            <td>10</td>
                                            <td>5</td>
                                        </tr>
                                    </tbody>
                                </Table>
                            </Accordion.Body>
                        </Accordion.Item>



                        <Accordion.Item eventKey="11" className='card shadow-sm border-0 mb-2 rounded'>
                            <Accordion.Header>12. Question Creator type wise Tests </Accordion.Header>
                            <Accordion.Body>
                                <Table bordered className='analytics-table mb-0 w-50'>
                                    <thead>
                                        <tr>
                                            <th className='table-light'>Type</th>
                                            <th className='table-success'>Test Count (%)</th>
                                            <th className='table-success'>Questions Count</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>                                       
                                        <tr>
                                            <td className='table-light text-start'>BP Team</td>
                                            <td>700 (70%)</td>
                                            <td>2000</td>                                           
                                        </tr>
                                        <tr>
                                            <td className='table-light text-start'>All School Teachers</td>
                                            <td>300 (30%)</td>
                                            <td>500</td>                                           
                                        </tr>
                                    </tbody>
                                </Table>
                            </Accordion.Body>
                        </Accordion.Item>

                    </Accordion>
                </div>

            </>

        )
    }
}
