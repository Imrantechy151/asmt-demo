import React, { Component } from 'react'
import {Form,Button,Image,Col,Row} from 'react-bootstrap';
import ApiService from '../../services/ApiServices';
import 'font-awesome/css/font-awesome.min.css';
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import { ThreeDots } from 'react-loader-spinner'
import {Link } from "react-router-dom";
import List from '../../component/Quiz/Quiz_Guest/List';

export default class Quiz_Page extends Component {
    constructor(props) {
        super(props)
        this.state = {
          quiz: [],
          isLoading: true
        }
      }
      async componentDidMount() {
        await ApiService.GetQuestionBank().then(res => {
          console.log(res.data);
          let result = res.data;
          if (result.status == 200) {
            this.setState({ quiz: result.response, isLoading: false })
          }
        }, error => {
          console.log(error);
        })
      }
    render() {
        const { quiz, isLoading } = this.state;
    if (isLoading) {
      return (
        <div style={{ position: "fixed", left: "50%", top: "30%", bottom: 0, right: "50%", width: "100%", zIndex: 9999 }}>
          <ThreeDots
            height="100"
            width="100"
            color='#EE4491'
            ariaLabel='loading'
          />
        </div>
      )
    }
        return (
            <div>
                {/* <div className='d-flex align-items-center justify-content-end mb-3'>
                    <h5 className='mb-0 me-2 small'>Select Quiz Type</h5>
                    <Form.Select className='w-25 select-type shadow-sm border-0' aria-label="Default select example">
                        <option>Select the Type</option>
                        <option value="1">Type 1</option>
                        <option value="2">Type 2</option>
                        <option value="3">Type 3</option>
                    </Form.Select>
                </div> */}

                <List data={quiz}/>
                {/* <div className='card shadow-sm p-md-4 p-3 border-0 questions-card'>
                    <h6>
                        <span>&#x25B7;</span> After a forest fire, there are 418 old trees, plus 223 baby trees called saplling.
                        How many trees are there in total?
                    </h6>
                    <div className='d-flex align-items-center mb-3'>
                        <Form.Control type="text" placeholder="" className='txtbox' /> Trees
                    </div>
                    <div>
                        <Button variant="success">Submit</Button>
                    </div>
                </div>

                <div className='card shadow-sm p-md-4 p-3 border-0 questions-card'>
                    <h6>
                        <span>&#x25B7;</span> Subtract multiples of 100
                    </h6>
                    <Row>
                        <Col md={6}>
                            <div>
                            <div className='mb-4'>
                                    <Image src="/images/marketing.png" className='img-fluid' width={25} />&nbsp; <span style={{fontWeight:'500'}}>Subtract :</span>
                                </div>
                                <div className='ms-3' style={{width:80}}>
                                    <div className='position-relative'>
                                        <p className='mb-1 text-end' style={{fontWeight:'500'}}>700</p>
                                        <span className='arithmatic-operator'>&#8722;</span><p className='mb-1 text-end' style={{fontWeight:'500'}}>100</p>
                                    </div>
                                    <hr style={{backgroundColor:'#000',opacity:1}} className='mb-2 mt-2' />
                                    <Form.Control type="text" placeholder="" className='form-control form-control-sm mb-3 text-end' />
                                    <Button variant="success">Submit</Button> 
                                </div>
                            </div>
                        </Col>
                        <Col md={6}>
                            <div>
                            <div className='mb-4'>
                                    <Image src="/images/marketing.png" className='img-fluid' width={25} />&nbsp; <span style={{fontWeight:'500'}}>Subtract :</span>
                                </div>
                                <div className='ms-3' style={{}}>
                                    <div className='position-relative mb-4'>
                                        <p className='mb-4' style={{fontWeight:'500',fontSize:20}}>700 <span>&#8722;</span> 100 = <input type="text" placeholder="" className='txtbox border rounded' /></p>                                        
                                    </div>
                                
                                    <Button variant="success" className='px-5'>Submit</Button> 
                                </div>
                            </div>
                        </Col>
                    </Row>
                </div>

                <div className='card shadow-sm p-md-4 p-3 border-0 questions-card'>
                    <h6>
                        <span>&#x25B7;</span> Subtract number up to three digits
                    </h6>
                    <Row>
                        <Col md={6}>
                            <div>
                            <div className='mb-4'>
                                    <Image src="/images/marketing.png" className='img-fluid' width={25} />&nbsp; <span style={{fontWeight:'500'}}>Subtract :</span>
                                </div>
                                <div className='ms-3' style={{width:80}}>
                                    <div className='position-relative'>
                                        <p className='mb-1 text-end' style={{fontWeight:'500'}}>786</p>
                                        <span className='arithmatic-operator'>&#8722;</span><p className='mb-1 text-end' style={{fontWeight:'500'}}>99</p>
                                    </div>
                                    <hr style={{backgroundColor:'#000',opacity:1}} className='mb-2 mt-2' />
                                    <Form.Control type="text" placeholder="" className='form-control form-control-sm mb-3 text-end' />
                                    <Button variant="success">Submit</Button> 
                                </div>
                            </div>
                        </Col>
                        <Col md={6}>
                            <div>
                            <div className='mb-4'>
                                    <Image src="/images/marketing.png" className='img-fluid' width={25} />&nbsp; <span style={{fontWeight:'500'}}>Subtract :</span>
                                </div>
                                <div className='ms-3' style={{}}>
                                    <div className='position-relative mb-4'>
                                        <p className='mb-4' style={{fontWeight:'500',fontSize:20}}>786 <span>&#8722;</span> 99 = <input type="text" placeholder="" className='txtbox border rounded' /></p>                                        
                                    </div>
                                
                                    <Button variant="success" className='px-5'>Submit</Button> 
                                </div>
                            </div>
                        </Col>
                    </Row>
                </div>

                <div className='card shadow-sm p-md-4 p-3 border-0 questions-card'>
                    <h6>
                        <span>&#x25B7;</span> Place value models - tens and ones
                    </h6>
                    <Row>
                        <Col md={6}>
                            <div>
                                <div className='mb-4'>
                                    <Image src="/images/marketing.png" className='img-fluid' width={25} />&nbsp; <span style={{fontWeight:'500'}}>Which place value model shows 50 ?</span>
                                </div>
                                <div className='d-flex mb-4'>
                                    <div>                                       
                                        <div className='overlay-checkbox'>
                                        <form class="form cf" action="/" method="post">
                                            <section class="plan cf"> 
                                                <div  className='image-question'>
                                                    <input type="radio" name="img" id="img1" value="img1" />
                                                    <label class="free-label four col" for="img1">
                                                        <Image src='/images/bar.png' />
                                                    </label>
                                                </div>
                                                <div  className='image-question'>
                                                    <input type="radio" name="img" id="img2" value="img2" />
                                                    <label class="free-label four col" for="img2">
                                                        <Image src='/images/bar2.png' />
                                                     </label>
                                                </div>
                                            </section>

                                        </form>                                                                                       
                                        </div>
                                    </div>
                                    
                                </div>
                                <Button variant="success" className='px-4'>Submit</Button> 
                            </div>
                        </Col>
                        
                    </Row>
                </div>

                <div className='card shadow-sm p-md-4 p-3 border-0 questions-card'>
                    <h6>
                        <span>&#x25B7;</span> Determine the main idea of a passage
                    </h6>
                    <span className='text-success'>Read the passage</span><br/>
                    <div className='bg-light p-md-3 p-2 mb-3'>
                        <Row>
                            <Col md={6}>
                                <div>                                
                                    <p className='mb-0'>
                                        Lorem ipsum dolor, sit amet consectetur adipisicing elit. Distinctio, ducimus repellat 
                                        similique at autem repudiandae, sequi accusantium accusamus laborum vitae earum corrupti 
                                        aspernatur expedita, recusandae reprehenderit? Amet provident corrupti magni.recusandae
                                        reprehenderit? Amet provident corrupti magni. aspernatur expedita, recusandae reprehenderit? Amet provident corrupti magni.recusandae
                                        reprehenderit? Amet provident corrupti magni.
                                    </p>
                                </div>
                            </Col>
                            <Col md={6}>
                                <div>                                
                                    <Image src="/images/new.png" className='img-fluid' />
                                </div>
                            </Col>                        
                        </Row>
                    </div>
                    <span className='text-success'>What is the main idea of the passage ?</span><br/>                    
                    <form class="form cf" action="/" method="post">
                        <section class="plan cf d-block"> 
                            <div className='mb-3'>
                                <input type="radio" name="img" id="img3" value="img3" />
                                <label class="free-label border-radio col" for="img3">
                                    Exercise helps people feel better in many ways.
                                </label>
                            </div>
                            <div>
                                <input type="radio" name="img" id="img4" value="img4" />
                                <label class="free-label border-radio col" for="img4">
                                    People who exercise regularly are happier.
                                </label>
                            </div>
                        </section>
                    </form>                    
                    <div className='mt-3'>
                        <Button variant="success" className='px-4'>Submit</Button>                         
                    </div>
                    
                </div> */}
            </div>
        )
    }
}