import React, { Component } from 'react'
import { Form, Button, Image, Col, Row, Table, Dropdown } from 'react-bootstrap';
import { Link } from 'react-router-dom'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import Quiz_Type_A from '../../component/Quiz/Quiz_Admin/Quiz_Type_A';
import Quiz_Type_B from '../../component/Quiz/Quiz_Admin/Quiz_Type_B';
import Quiz_Type_C from '../../component/Quiz/Quiz_Admin/Quiz_Type_C';
import Quiz_Type_D from '../../component/Quiz/Quiz_Admin/Quiz_Type_D';
import Types from '../../helper/type.json'
import Multiselect from 'multiselect-react-dropdown';

export default class QuestionType extends Component {
    constructor(props) {
        super(props)
        this.state = {
            type: null, subject: null,
            std: null,
            QBMGroup: null, QBMLearningCode: [],

            QBMLearningCodeData: [
                { name: "ACEAD ACELY1713", id: "ACEAD ACELY1713" },
                { name: "ACEAD ACELY1660", id: "ACEAD ACELY1660" },
                { name: "ACEAD ACELY1661", id: "ACEAD ACELY1661" }
            ],
            direction: "Horizontal",
            option: "2",
            QBMTimerValue: null,
            name:""
        }

    }
    onSelect(selectedList, selectedItem) {
        console.log(selectedList)
        const QBMLearningCode = selectedList.map((item => {
            return item.name;
        }))
        this.setState({ QBMLearningCode })
        // console.log(QBMLearningCode)
    }

    onRemove(selectedList, removedItem) {
        //console.log(selectedList)
        let QBMLearningCode = selectedList.map((item => {
            return item.name;
        }))
        //this.setState({QBMLearningCode})
        // this.setState({QBMLearningCode})
        //  console.log(QBMLearningCode)
    }
    SetQBMLearningCode = (QBMLearningCode) => {
        console.log(QBMLearningCode)
    }
    render() {
        const { type, std, direction, option, subject, QBMLearningCode,name } = this.state;
        console.log(QBMLearningCode)
        return (
            <div>
                <ToastContainer />
                <Row className=''>
                    <Col md={type == null || type == "" ? 12 : 12}>
                        <div className='text-end mb-3 d-md-flex justify-content-between align-items-center'>
                            <h5 className='mb-0 text-theme'>Question Types</h5>
                        </div>
                        <div className='card p-md-3 p-3 shadow-sm border-0 mb-3 create-question'>
                            <Row className='align-items-center'>
                                                             
                                <Col md={type == null || type == "" ? 4 : 4}>
                                    <Form.Group className="" controlId="formBasicEmail">
                                        {/* <Form.Label>Question Type</Form.Label> */}
                                        <Form.Select onChange={(e) => {
                                            this.setState({ type: e.target.value, direction: "Horizontal", option: "2" })
                                                  var nm=Types.find(a=>a.value==e.target.value);
                                                  if(nm!=null){
                                                    this.setState({name:nm.name})
                                                  }
                                        }} aria-label="Default select example" className="small">
                                            <option value="">Question Type</option>
                                            {Types.map((item => (
                                                <option value={item.value}>{item.id} - {item.name}</option>
                                            )))}

                                        </Form.Select>
                                    </Form.Group>
                                </Col>

                                <Col md={6} className="mx-auto mb-4" >
                        {type == "A" ?

                            <div className='mb-3 position-sticky top-0'>
                                <h5 className='mb-2 text-center mb-3'>{name}</h5>
                                <div className='computer-screen h-100 bg-white'>
                                    <img src="/images/img1.jpg" className='img-fluid ' />
                                </div>
                            </div>
                            : null
                        }
                        {(type == "B") && (direction == "Horizontal") ?
                            <div className='mb-3 position-sticky top-0'>
                                <h5 className='mb-2 text-center mb-3'>{name}</h5>
                                <div className='computer-screen h-100'>
                                    <img src="/images/img9.jpg" className='img-fluid' />
                                </div>
                            </div>
                            : null
                        }
                        {(type == "B") && (direction == "Vertical") ?
                            <div className='mb-3 position-sticky top-0'>
                                <h5 className='mb-2 text-center mb-3'>{name}</h5>
                                <div className='computer-screen h-100'>
                                    <img src="/images/img10.jpg" className='img-fluid' />
                                </div>
                            </div>
                            : null
                        }
                        {(type == "BA") && (direction == "Horizontal") ?
                            <div className='mb-3 position-sticky top-0'>
                                <h5 className='mb-2 text-center mb-3'>{name}</h5>
                                <div className='computer-screen h-100'>
                                    <img src="/images/img3.jpg" className='img-fluid' />
                                </div>
                            </div>
                            : null
                        }
                        {(type == "BA") && (direction == "Vertical") ?
                            <div className='mb-3 position-sticky top-0'>
                                <h5 className='mb-2 text-center mb-3'>{name}</h5>
                                <div className='computer-screen h-100'>
                                    <img src="/images/img2.jpg" className='img-fluid' />
                                </div>
                            </div>
                            : null
                        }

                        {(type == "BB") && (direction == "Horizontal") ?
                            <div className='mb-3 position-sticky top-0'>
                                <h5 className='mb-2 text-center mb-3'>{name}</h5>
                                <div className='computer-screen h-100'>
                                    <img src="/images/img11.jpg" className='img-fluid' />
                                </div>
                            </div>
                            : null
                        }
                        {(type == "BB") && (direction == "Vertical") ?
                            <div className='mb-3 position-sticky top-0'>
                                <h5 className='mb-2 text-center mb-3'>{name}</h5>
                                <div className='computer-screen h-100'>
                                    <img src="/images/img12.jpg" className='img-fluid' />
                                </div>
                            </div>
                            : null
                        }
                        {(type == "BC") && (direction == "Horizontal") ?
                            <div className='mb-3 position-sticky top-0'>
                                <h5 className='mb-2 text-center mb-3'>{name}</h5>
                                <div className='computer-screen h-100'>
                                    <img src="/images/img13.jpg" className='img-fluid' />
                                </div>
                            </div>
                            : null
                        }
                        {(type == "BC") && (direction == "Vertical") ?
                            <div className='mb-3 position-sticky top-0'>
                                <h5 className='mb-2 text-center mb-3'>{name}</h5>
                                <div className='computer-screen h-100'>
                                    <img src="/images/img14.jpg" className='img-fluid' />
                                </div>
                            </div>
                            : null
                        }
                        {type === "C" && option === "2" ?
                            <div className='mb-3 position-sticky top-0'>
                                <h5 className='mb-2 text-center mb-3'>{name}</h5>
                                <div className='computer-screen h-100'>
                                    <img src="/images/img4.jpg" className='img-fluid' />
                                </div>
                            </div>
                            : null
                        }
                        {type === "C" && option === "3" ?
                            <div className='mb-3 position-sticky top-0'>
                                <h5 className='mb-2 text-center mb-3'>{name}</h5>
                                <div className='computer-screen h-100'>
                                    <img src="/images/img7.jpg" className='img-fluid' />
                                </div>
                            </div>
                            : null
                        }
                        {type == "D" ?
                            <div className='mb-3 position-sticky top-0' >
                                <h5 className='mb-2 text-center mb-3'>{name}</h5>
                                <div className='computer-screen h-100'>
                                    <img src="/images/img6.jpg" className='img-fluid' />
                                </div>
                            </div>
                            : null
                        }

                    </Col>
                            </Row>
                        </div>

                    </Col>


                    
                </Row>
            </div >
        )
    }
}
