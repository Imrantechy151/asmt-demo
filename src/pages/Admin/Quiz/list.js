import React, { Component } from 'react'
import { Button, Col, Form, Image, Row, Modal } from 'react-bootstrap'
import Url from '../../../helper/url';
import Types from '../../../helper/type.json'



export default class list extends Component {
    constructor(props) {
        super(props)
        this.state = {
            IsModel: false,
            explanation: ""
        }
    }
    render() {
        const { data } = this.props
        const { IsModel } = this.state;

        const CommonData = ({ type, title, std, subject }) => {
            return (
                <>
                    <div className='d-md-flex align-items-center justify-content-between'>
                        <div style={{ flexBasis: '80%' }}>
                            <h6>
                                {type == "D" ? "Determine the main idea of a passage" :
                                type == "C" ? "Place value models - tens and ones" : title}
                            </h6>
                        </div>
                    </div>
                </>
            )
        }
        const CommonData1 = ({ type, std, subject, code }) => {
            var tp = Types.find(a => a.value == type);
            return (
                <div className='card-header d-md-flex justify-content-between bg-white p-3' style={{ borderBottomColor: '#f0f0f0' }}>
                    <div>
                        <small className='me-2 text-secondary fw-normal '>Created by <b className='text-secondary' style={{ fontWeight: '600' }}>David Hales (Admin)</b></small>
                        <span className='badge btn-theme text-white fw-normal me-2' data-bs-toggle="tooltip" title='Question Type'>{tp ? tp.name : ""}</span>
                    </div>
                    <div>
                        <span className='badge bg-color1 text-dark fw-normal me-2' title='Year Level'> {std}</span>
                        <span className='badge bg-color1 text-dark me-2 fw-normal' title="Subject"> {subject}</span>
                        <span className='badge bg-color1 text-dark me-2 fw-normal' title='LeaningCode'>ACEAD ACELY1713</span>
                        <span className='badge bg-color1 text-dark fw-normal me-2' title='Toughness'>Easy</span>
                        <span className='badge bg-color1 text-dark fw-normal me-2' title='Question Code'>{code}</span>
                        <a href='#' className='px-2 text-decoration-none small'>
                            <i className='fa fa-pencil'></i>
                        </a>
                    </div>
                </div>
            )
        }

        const CommonData2 = ({ type, title, std, subject, timer, explanation }) => {
            return (
                <div className='card-footer px-4 d-md-flex align-items-center justify-content-between p-3 border-0' style={{ backgroundColor: '#f7fcfc' }}>
                    <div>
                        <span className='btn-link text-primary text-decoration-none small' onClick={() => { this.setState({ IsModel: true, explanation: explanation }) }} style={{ color: 'gray', cursor: 'pointer' }}>View Explanation  </span> &nbsp;
                    </div>
                    <div>                        
                        <span className='small badge bg-color1  fw-normal' style={{ color: 'gray' }}>
                            Student Answers : <span><i className='fa fa-check text-success'></i></span> 30(15%) &nbsp; &nbsp; <span className='text-danger fw-bold'><i className='fa fa-close'></i></span> 75(85%)                            
                        </span>
                        &nbsp;<span className='small badge bg-color1  fw-normal' style={{ color: 'gray' }}>Total Assigned In <b>23</b> Assignments  &  <b>50</b> Students</span>&nbsp;
                        <span className='badge bg-color1 text-gray small  fw-normal' style={{ color: 'gray' }}>Teachers Feedback : <span className='text-success'> <i class="fa fa-thumbs-o-up"></i> 35</span> &nbsp;&nbsp; <span className='text-danger'>  <i class="fa fa-thumbs-o-down"></i> 15</span> </span>                        
                    </div>
                </div>
            )
        }

        return (
            <div>
                <Modal size='lg' show={IsModel} onHide={() => { this.setState({ IsModel: false }) }}>
                    <Modal.Header closeButton>
                        <Modal.Title>Explanation</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>

                        <div dangerouslySetInnerHTML={{ __html: this.state.explanation }}></div>
                    </Modal.Body>

                </Modal>
                {data.map((item) => {
                    if (item.QBMType == "A") {
                        return (
                            <>
                                <div className='card shadow-sm border-0 questions-card'>
                                    {<CommonData1 type={item.QBMType} std={item.QBMStd} subject={item.QBMSubject} code={item._id} />}
                                    <div className='card-body p-md-4 p-3 '>
                                        {<CommonData title={item.QBMDetail.QBMDQuestionTitle} std={item.QBMStd} subject={item.QBMSubject} />}
                                        <div className='d-flex align-items-center mb-3'>
                                            <Form.Control type="text" placeholder="" className='txtbox' name="ans" /> {item.QBMDetail.QBMDWordAfterBlank}
                                        </div>
                                    </div>
                                    {<CommonData2 explanation={item.QBMExplanation} timer={item.QBMTimerValue} />}
                                </div>
                            </>
                        )
                    }
                    else if (item.QBMType == "B" || item.QBMType == "BA" || item.QBMType == "BB" || item.QBMType == "BC") {
                        return (
                            <div className='card shadow-sm border-0 questions-card'>
                                {/* <h6>
                                    <span>&#x25B7;</span> {item.QBMDetail.QBMDQuestionTitle}
                                </h6> */}
                                {<CommonData1 type={item.QBMType} std={item.QBMStd} subject={item.QBMSubject} code={item._id} />}
                                <div className='card-body p-md-4 p-3 '>
                                    {<CommonData title={item.QBMDetail.QBMDQuestionTitle} std={item.QBMStd} subject={item.QBMSubject} />}
                                    <Row>
                                        {item.QBMDetail.QBMDDirection == "Vertical"
                                            ?
                                            <Col md={12}>
                                                <div>
                                                    <div className='mb-4'>
                                                        <span style={{ fontWeight: '500' }}>{item.QBMDetail.QBMDOperator == "-" ? "Subtract" : item.QBMDetail.QBMDOperator == "+" ?
                                                            "Addition" : item.QBMDetail.QBMDOperator == "x" ? "Multiplication" : item.QBMDetail.QBMDOperator == "/" ? "Division" : ""} :</span>
                                                    </div>
                                                    <div className='' style={{ width: 80 }}>
                                                        <div className='position-relative'>
                                                            <p className='mb-1 text-end' style={{ fontWeight: '500' }}>{item.QBMDetail.QBMDOperand1}</p>
                                                            <span className='arithmatic-operator'>{item.QBMDetail.QBMDOperator}</span><p className='mb-1 text-end' style={{ fontWeight: '500' }}>{item.QBMDetail.QBMDOperand2}</p>
                                                        </div>
                                                        <hr style={{ backgroundColor: '#000', opacity: 1 }} className='mb-2 mt-2' />
                                                        <Form.Control type="text" placeholder="" className='form-control form-control-sm mb-3 text-end' />

                                                    </div>
                                                </div>
                                            </Col>
                                            :
                                            <Col md={12}>
                                                <div>
                                                    <div className='mb-4'>
                                                        <span style={{ fontWeight: '500' }}>{item.QBMDetail.QBMDOperator == "-" ? "Subtract" : item.QBMDetail.QBMDOperator == "+" ?
                                                            "Addition" : item.QBMDetail.QBMDOperator == "x" ? "Multiplication" : item.QBMDetail.QBMDOperator == "/" ? "Division" : ""} :</span>
                                                    </div>
                                                    <div className='' style={{}}>
                                                        <div className='position-relative mb-4'>
                                                            <p className='mb-4' style={{ fontWeight: '500', fontSize: 20 }}>{item.QBMDetail.QBMDOperand1} <span>{item.QBMDetail.QBMDOperator}</span> {item.QBMDetail.QBMDOperand2} = <input type="text" placeholder="" className='txtbox border rounded' /></p>
                                                        </div>


                                                    </div>
                                                </div>
                                            </Col>
                                        }
                                    </Row>

                                </div>
                                {<CommonData2 explanation={item.QBMExplanation} timer={item.QBMTimerValue} />}
                            </div>
                        )
                    }
                    else if (item.QBMType == "C") {
                        return (
                            <div className='card shadow-sm border-0 questions-card'>
                                {/* <h6>
                                    <span>&#x25B7;</span> Place value models - tens and ones
                                </h6> */}
                                {<CommonData1 type={item.QBMType} std={item.QBMStd} subject={item.QBMSubject} code={item._id} />}
                                <div className='card-body p-md-4 p-3 '>
                                    {<CommonData type={item.QBMType} title={item.QBMDetail.QBMDQuestionTitle} std={item.QBMStd} subject={item.QBMSubject} />}
                                    <Row>
                                        <Col md={6}>
                                            <div>
                                                <div className='mb-4'>
                                                    <span style={{ fontWeight: '500' }}>{item.QBMDetail.QBMDQuestionTitle}</span>
                                                </div>
                                                <div className='d-flex mb-4'>
                                                    <div>
                                                        <div className='overlay-checkbox'>
                                                            <form class="form cf" action="/" method="post">
                                                                <section class="plan cf">
                                                                    {/* http://localhost:4000/uploads/question/bank/ */}
                                                                    {item.QBMDetail.QBMDOptions.map((img, key) => {
                                                                        return (
                                                                            <div className='image-question'>
                                                                                <input type="radio" name="img" id={img} value={"Option" + key} />
                                                                                <label class="free-label four col" for={img}>
                                                                                    <Image src={Url.FileUrl() + img} className="img-fluid" />
                                                                                </label>
                                                                            </div>
                                                                        )
                                                                    })}
                                                                    {/* <div className='image-question'>
                                                                    <input type="radio" name="img" id="img1" value="img1" />
                                                                    <label class="free-label four col" for="img1">
                                                                        <Image src='/images/bar.png' />

                                                                    </label>
                                                                </div>
                                                                <div className='image-question'>
                                                                    <input type="radio" name="img" id="img2" value="img2" />
                                                                    <label class="free-label four col" for="img2">
                                                                        <Image src='/images/bar2.png' />
                                                                    </label>
                                                                </div> */}
                                                                </section>

                                                            </form>
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>
                                        </Col>

                                    </Row>
                                </div>
                                {<CommonData2 explanation={item.QBMExplanation} timer={item.QBMTimerValue} />}

                            </div>
                        );
                    }
                    else if (item.QBMType == "D") {
                        return (
                            <div className='card shadow-sm border-0 questions-card'>
                                {/* <h6>
                                <span>&#x25B7;</span> Place value models - tens and ones
                            </h6> */}
                                {<CommonData1 type={item.QBMType} std={item.QBMStd} subject={item.QBMSubject} code={item._id} />}
                                <div className='card-body p-md-4 p-3 '>

                                    {<CommonData type={item.QBMType} title={item.QBMDetail.QBMDQuestionTitle} std={item.QBMStd} subject={item.QBMSubject} />}
                                    <span className='text-success'>Read the passage</span><br />
                                    <div className='bg-light p-md-3 p-2 mb-3'>
                                        <Row>
                                            <Col md={6}>
                                                <div>
                                                    <h6 className='text-center mb-1'>{item.QBMDetail.QBMDQuestionTitle}</h6>

                                                    <p className='mb-0' dangerouslySetInnerHTML={{ __html: item.QBMDetail.QBMDPassage }}></p>
                                                </div>
                                            </Col>
                                            <Col md={6}>
                                                <div>

                                                    <Image src={Url.FileUrl() + item.QBMDetail.QBMDPassageImage} className='img-fluid' style={{ height: 200, objectFit: "cover" }} />
                                                </div>
                                            </Col>
                                        </Row>
                                    </div>
                                    <span className='text-success'>{item.QBMDetail.QBMDOptionTitle}</span><br />
                                    <form class="form cf" action="/" method="post">
                                        <section class="plan cf d-block">
                                            {item.QBMDetail.QBMDOptions.map((o, key) => {
                                                return (
                                                    <div className={"mb-2"}>
                                                        <input type="radio" name="img" id={"op" + o} value={"Option" + key} />
                                                        <label class="free-label border-radio col" for={"op" + o}>
                                                            {o}
                                                        </label>
                                                    </div>
                                                )
                                            })}
                                            {/* <div>
                                        <input type="radio" name="img" id="img4" value="img4" />
                                        <label class="free-label border-radio col" for="img4">
                                            People who exercise regularly are happier.
                                        </label>
                                    </div> */}
                                        </section>
                                    </form>

                                </div>
                                {<CommonData2 explanation={item.QBMExplanation} timer={item.QBMTimerValue} />}

                            </div>
                        )
                    }
                    else {
                        return null
                    }
                })}
            </div>
        )
    }
}
