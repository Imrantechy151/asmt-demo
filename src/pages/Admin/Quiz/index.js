import React, { Component } from 'react'
import { Button, Table, Dropdown,Row,Col,Form } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import 'font-awesome/css/font-awesome.min.css';
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import ApiService from '../../../services/ApiServices';
import { ThreeDots } from 'react-loader-spinner'
import List from './list';
import Type from '../../../helper/type.json';


export default class QuizList extends Component {
  constructor(props) {
    super(props)
    this.handleChange = this.handleChange.bind(this);
    this.state = {
      quiz: [],
      isLoading: true,
      checked: false,      
    }
  }
  async componentDidMount() {
    await ApiService.GetQuestionBank().then(res => {
      console.log(res.data);
      let result = res.data;
      if (result.status == 200) {
        this.setState({ quiz: result.response, isLoading: false })
      }
    }, error => {
      console.log(error);
    })
  }

  handleChange() {
    this.setState({
      checked: !this.state.checked
    })
  }

  render() {
    const { quiz, isLoading,checked } = this.state;
    const content = this.state.checked 
    	? <div className='d-flex align-items-end'> 
          <Form.Control type="text" placeholder="(Year level=1 or 2 or 3) And (Subject=English) And (Toughnesslevel=hard)" className='form-control form-control-sm mt-2' />  
          <a href='#' className='btn btn-theme-secondary d-flex align-items-center justify-content-center border-0 ms-4' style={{height:45,width:67}}>Find</a>
        </div>
      : null;

    if (isLoading) {
      return (
        <div style={{ position: "fixed", left: "50%", top: "30%", bottom: 0, right: "50%", width: "100%", zIndex: 9999 }}>
          <ThreeDots
            height="100"
            width="100"
            color='#EE4491'
            ariaLabel='loading'
          />
        </div>
      )
    }
    return (
      <div>
        <div className='sticky-header'>
        <div className='card p-3 py-2 shadow-sm border-0 mb-2'>
          <div className='text-end d-md-flex justify-content-between align-items-center'>
            <h5 className='mb-0 text-secondary'>Questions List <small className="text-theme" style={{fontSize:15}}>(850/1000)</small></h5>
            <div>
                <button className='btn btn-link btn-sm text-secondary text-decoration-none'>Student Answers <i class="fa fa-long-arrow-down text-theme-blue small"></i><i class="fa fa-long-arrow-up text-theme-blue small"></i></button>
                <button className='btn btn-link  btn-sm text-secondary text-decoration-none'>Teachers Feedback <i class="fa fa-long-arrow-down text-theme-blue small"></i><i class="fa fa-long-arrow-up text-theme-blue small"></i></button>                  

                <Link to='/Admin/Question/Create' className='btn btn-theme btn-sm ms-3'>Create New Question</Link>
            </div>
            
          </div>          
        </div>
{!checked?
        <div className='card p-3 shadow-sm border-0 mb-1'>
            <h6 className='mb-1 text-secondary'>Filter Questions</h6>
            <Row className='align-items-end'>
              <Col md={2}>
                <Form.Group className="mb-0" controlId="formBasicEmail">
                  <Form.Label>Year Level</Form.Label>
                  <Form.Select>
                    <option>Select year level</option>
                    <option>5</option>
                    <option>6</option>
                    <option>7</option>
                  </Form.Select>                
                </Form.Group>
              </Col>
              <Col md={2}>
                <Form.Group className="mb-0" controlId="formBasicEmail">
                  <Form.Label>Subject</Form.Label>
                  <Form.Select>
                    <option>Select Subject</option>
                    <option>English</option>
                    <option>Maths</option>
                    <option>Science</option>
                  </Form.Select>                
                </Form.Group>
              </Col>
              <Col md={3}>
                <Form.Group className="mb-0" controlId="formBasicEmail">
                  <Form.Label>Question Type</Form.Label>
                  <Form.Select>
                    <option>Select question type</option>
                    {Type.map((item)=>
                      {
                        return (
                          <option value={item.value}>{item.id} - {item.name}</option>
                        );
                      }
                      )}                                     
                  </Form.Select>                
                </Form.Group>
              </Col>
              <Col md={2}>
                <Form.Group className="mb-0" controlId="formBasicEmail">
                  <Form.Label>Toughness Level</Form.Label>
                  <Form.Select>
                    <option>Tooghness level</option>
                    <option>Easy</option>
                    <option>Medium</option>
                    <option>Hard</option>
                  </Form.Select>                
                </Form.Group>
              </Col>
              <Col md={2}>
                <Form.Group className="mb-0" controlId="formBasicEmail">
                  <Form.Label>Question Code </Form.Label>
                  <Form.Control type="text" placeholder="Enter Question Code" className='form-control form-control-sm' />                
                </Form.Group>
              </Col>
              <Col md={1}>
                    <a href='#' className='btn btn-theme-secondary w-100 d-flex align-items-center justify-content-center border-0' style={{height:45}}>Find</a>
              </Col>
            </Row>                        
        </div>
        :null}
        <div className='card p-3 shadow-sm border-0'>
            <h6 className='text-secondary mb-0'>Query Filter <input type="checkbox" className='qlist qlist-checked' checked={ this.state.checked } onChange={ this.handleChange } /></h6>
            { content }            
        </div>  
        </div>
        <div className='mt-2'>         
          <List data={quiz} />
        </div>
      </div>
    )
  }
}
