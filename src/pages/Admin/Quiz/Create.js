import React, { Component } from 'react'
import { Form, Button, Image, Col, Row, Table, Dropdown } from 'react-bootstrap';
import { Link } from 'react-router-dom'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import Quiz_Type_A from '../../../component/Quiz/Quiz_Admin/Quiz_Type_A';
import Quiz_Type_B from '../../../component/Quiz/Quiz_Admin/Quiz_Type_B';
import Quiz_Type_C from '../../../component/Quiz/Quiz_Admin/Quiz_Type_C';
import Quiz_Type_D from '../../../component/Quiz/Quiz_Admin/Quiz_Type_D';
import Types from '../../../helper/type.json'
import Multiselect from 'multiselect-react-dropdown';

export default class Create extends Component {
    constructor(props) {
        super(props)
        this.state = {
            type: null, subject: null,
            std: null,
            QBMGroup: null, QBMLearningCode: [],

            QBMLearningCodeData: [
                { name: "ACEAD ACELY1713", id: "ACEAD ACELY1713" },
                { name: "ACEAD ACELY1660", id: "ACEAD ACELY1660" },
                { name: "ACEAD ACELY1661", id: "ACEAD ACELY1661" }
            ],
            direction: "Horizontal",
            option: "2",
            QBMTimerValue: null,
        }

    }
    onSelect(selectedList, selectedItem) {
        console.log(selectedList)
        const QBMLearningCode = selectedList.map((item => {
            return item.name;
        }))
        this.setState({ QBMLearningCode })
        // console.log(QBMLearningCode)
    }

    onRemove(selectedList, removedItem) {
        //console.log(selectedList)
        let QBMLearningCode = selectedList.map((item => {
            return item.name;
        }))
        //this.setState({QBMLearningCode})
        // this.setState({QBMLearningCode})
        //  console.log(QBMLearningCode)
    }
    SetQBMLearningCode = (QBMLearningCode) => {
        console.log(QBMLearningCode)
    }
    render() {
        const { type, std, direction, option, subject, QBMLearningCode } = this.state;
        console.log(QBMLearningCode)
        return (
            <div>
                <ToastContainer />
                <Row className=''>
                    <Col md={type == null || type == "" ? 12 : 12}>
                        <div className='text-end mb-3 d-md-flex justify-content-between align-items-center'>
                            <h5 className='mb-0 text-theme'>Create Question</h5>
                        </div>
                        <div className='card p-md-3 p-3 shadow-sm border-0 mb-3 create-question'>
                            <Row className='align-items-center'>
                               
                                <Col md={2}>
                                    <Form.Group className="" controlId="formBasicEmail">
                                        <Form.Label>Subject</Form.Label>
                                        <Form.Select onChange={(e) => { this.setState({ subject: e.target.value }) }} className='' aria-label="Default select example">
                                            <option value="">Select Subject</option>
                                            <option value="Math">Math</option>
                                            <option value="English">English</option>
                                            <option value="Science">Science</option>
                                        </Form.Select>
                                    </Form.Group>
                                </Col>
                                <Col md={1}>
                                    <Form.Group className="" controlId="formBasicEmail">
                                        <Form.Label>Year Level</Form.Label>
                                        <Form.Select onChange={(e) => { this.setState({ std: e.target.value }) }} className='' aria-label="Default select example">
                                            <option value="">Year</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                        </Form.Select>
                                    </Form.Group>
                                </Col>
                                <Col md={2}>
                                    <Form.Group className="" controlId="formBasicEmail">
                                        <Form.Label>Toughness</Form.Label>
                                        <Form.Select onChange={(e) => { this.setState({ QBMGroup: e.target.value }) }} className='' aria-label="Default select example">
                                            <option value="">Toughness level</option>
                                            <option value="Easy">Easy</option>
                                            <option value="Medium">Medium</option>
                                            <option value="Hard">Hard</option>
                                        </Form.Select>
                                    </Form.Group>
                                </Col>

                                <Col md={5}>
                                    <Form.Group className="" controlId="formBasicEmail">
                                        <Form.Label>Learning Code</Form.Label>
                                        <Multiselect
                                            options={this.state.QBMLearningCodeData} // Options to display in the dropdown
                                            selectedValues={this.state.QBMLearningCode} // Preselected value to persist in dropdown
                                            onSelect={this.onSelect} // Function will trigger on select event
                                            onRemove={this.onRemove} // Function will trigger on remove event
                                            displayValue="id" // Property name to display in the dropdown options
                                        />
                                       
                                    </Form.Group>
                                </Col>

                                <Col md={type == null || type == "" ? 2 : 2}>
                                    <Form.Group className="" controlId="formBasicEmail">
                                        <Form.Label>Question Type</Form.Label>
                                        <Form.Select onChange={(e) => {
                                            this.setState({ type: e.target.value, direction: "Horizontal", option: "2" })

                                        }} aria-label="Default select example" className="small">
                                            <option value="">Question Type</option>
                                            {Types.map((item => (
                                                <option value={item.value}>{item.id} - {item.name}</option>
                                            )))}

                                        </Form.Select>
                                    </Form.Group>
                                </Col>
                            </Row>
                        </div>

                    </Col>

                    <Col md={7}>

                        {type == "A" ?
                            <div className='card card-height p-md-4 p-3 shadow-sm'>
                                <Form>
                                    <Quiz_Type_A props={this.props} type={type} std={std} subject={subject} QBMTimerValue={this.state.QBMTimerValue} QBMGroup={this.state.QBMGroup} QBMLearningCode={this.state.QBMLearningCode} />
                                </Form>
                            </div>
                            : null

                        }
                        {type == "B" || type == "BA" || type == "BB" || type == "BC" ?
                            <div className='card card-height p-md-4 p-3 shadow-sm'>
                                <Form>
                                    <Quiz_Type_B type={type} std={std} subject={subject} QBMGroup={this.state.QBMGroup} QBMTimerValue={this.state.QBMTimerValue} QBMLearningCode={this.state.QBMLearningCode} operator={type == "B" ? "+" : type == "BA" ? "-" : type == "BB" ? "x" : type == "BC" ? "/" : ""} ChangeDirection={(d) => { this.setState({ direction: d }) }} />
                                </Form>
                            </div>
                            : null
                        }
                        {type == "C" ?
                            <div className='card card-height p-md-4 p-3 shadow-sm'>
                                <Form>
                                    <Quiz_Type_C type={type} std={std} subject={subject} QBMGroup={this.state.QBMGroup} QBMLearningCode={this.state.QBMLearningCode} QBMTimerValue={this.state.QBMTimerValue} changeOption={(e) => { this.setState({ option: e }) }} />
                                </Form>
                            </div>
                            : null
                        }
                        {type == "D" ?
                            <div className='card card-height p-md-4 p-3 shadow-sm'>
                                <Form>
                                    <Quiz_Type_D type={type} std={std} subject={subject} QBMGroup={this.state.QBMGroup} QBMLearningCode={this.state.QBMLearningCode} QBMTimerValue={this.state.QBMTimerValue} />
                                </Form>
                            </div>
                            : null
                        }
                    </Col>

                    <Col md={5}>
                        {type == "A" ?

                            <div className='position-sticky top-0'>
                                <h6 className='mb-2 text-uppercase mb-3'>Preview</h6>
                                <div className='computer-screen h-100'>
                                    <img src="/images/img1.jpg" className='img-fluid' />
                                </div>
                            </div>
                            : null
                        }
                        {(type == "B") && (direction == "Horizontal") ?
                            <div className='mb-3 position-sticky top-0'>
                                <h6 className='mb-2 text-uppercase mb-3'>Preview</h6>
                                <div className='computer-screen h-100'>
                                    <img src="/images/img9.jpg" className='img-fluid' />
                                </div>
                            </div>
                            : null
                        }
                        {(type == "B") && (direction == "Vertical") ?
                            <div className='mb-3 position-sticky top-0'>
                                <h6 className='mb-2 text-uppercase mb-3'>Preview</h6>
                                <div className='computer-screen h-100'>
                                    <img src="/images/img10.jpg" className='img-fluid' />
                                </div>
                            </div>
                            : null
                        }
                        {(type == "BA") && (direction == "Horizontal") ?
                            <div className='mb-3 position-sticky top-0'>
                                <h6 className='mb-2 text-uppercase mb-3'>Preview</h6>
                                <div className='computer-screen h-100'>
                                    <img src="/images/img3.jpg" className='img-fluid' />
                                </div>
                            </div>
                            : null
                        }
                        {(type == "BA") && (direction == "Vertical") ?
                            <div className='mb-3 position-sticky top-0'>
                                <h6 className='mb-2 text-uppercase mb-3'>Preview</h6>
                                <div className='computer-screen h-100'>
                                    <img src="/images/img2.jpg" className='img-fluid' />
                                </div>
                            </div>
                            : null
                        }

                        {(type == "BB") && (direction == "Horizontal") ?
                            <div className='mb-3 position-sticky top-0'>
                                <h6 className='mb-2 text-uppercase mb-3'>Preview</h6>
                                <div className='computer-screen h-100'>
                                    <img src="/images/img11.jpg" className='img-fluid' />
                                </div>
                            </div>
                            : null
                        }
                        {(type == "BB") && (direction == "Vertical") ?
                            <div className='mb-3 position-sticky top-0'>
                                <h6 className='mb-2 text-uppercase mb-3'>Preview</h6>
                                <div className='computer-screen h-100'>
                                    <img src="/images/img12.jpg" className='img-fluid' />
                                </div>
                            </div>
                            : null
                        }
                        {(type == "BC") && (direction == "Horizontal") ?
                            <div className='mb-3 position-sticky top-0'>
                                <h6 className='mb-2 text-uppercase mb-3'>Preview</h6>
                                <div className='computer-screen h-100'>
                                    <img src="/images/img13.jpg" className='img-fluid' />
                                </div>
                            </div>
                            : null
                        }
                        {(type == "BC") && (direction == "Vertical") ?
                            <div className='mb-3 position-sticky top-0'>
                                <h6 className='mb-2 text-uppercase mb-3'>Preview</h6>
                                <div className='computer-screen h-100'>
                                    <img src="/images/img14.jpg" className='img-fluid' />
                                </div>
                            </div>
                            : null
                        }
                        {type === "C" && option === "2" ?
                            <div className='mb-3 position-sticky top-0'>
                                <h6 className='mb-2 text-uppercase mb-3'>Preview</h6>
                                <div className='computer-screen h-100'>
                                    <img src="/images/img4.jpg" className='img-fluid' />
                                </div>
                            </div>
                            : null
                        }
                        {type === "C" && option === "3" ?
                            <div className='mb-3 position-sticky top-0'>
                                <h6 className='mb-2 text-uppercase mb-3'>Preview</h6>
                                <div className='computer-screen h-100'>
                                    <img src="/images/img7.jpg" className='img-fluid' />
                                </div>
                            </div>
                            : null
                        }
                        {type == "D" ?
                            <div className='mb-3 position-sticky' style={{ top: 15 }}>
                                <h6 className='mb-2 text-uppercase mb-3'>Preview</h6>
                                <div className='computer-screen h-100'>
                                    <img src="/images/img6.jpg" className='img-fluid' />
                                </div>
                            </div>
                            : null
                        }

                    </Col>
                </Row>
            </div >
        )
    }
}
