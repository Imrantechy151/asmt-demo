import React, { Component } from 'react'
import { Outlet, Link } from "react-router-dom";

export default class Home_Page extends Component {
  render() {
    return (
<div>
      <h1>home-page</h1>
      <nav
        style={{
          borderBottom: "solid 1px",
          paddingBottom: "1rem",
        }}
      >
       
      </nav>
      <Outlet />
     
      </div>
    )
  }
}
