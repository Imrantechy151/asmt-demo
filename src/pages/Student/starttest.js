import React, { Component } from 'react';
import { Container, Row, Col, Form, Image, Modal } from 'react-bootstrap';
import ApiService from '../../services/ApiServices';
import Question from '../../helper/question.json';
import { ThreeDots } from 'react-loader-spinner'
import Url from '../../helper/url';
import Types from '../../helper/type.json';

export default class starttest extends Component {

    state = {
        minutes: 30,
        seconds: 0,
        question: 1,
        Questions: null,
        questionLoad: true,
        type: "Fill in the Blank",
        IsModel: false,
    }

    GetQuestion = (id) => {
        ApiService.GetQuestionBankDetails(id).then(res => {
            console.log(res.data)
            if (res.data.message === "success") {
                this.setState({ Questions: res.data.response, questionLoad: false })
            }
        })
    }
    NextQuestion = () => {
        if (this.state.question <= 5) {
            var ids = this.state.question + 1;
            var id = Question.Question.find(a => a.Id == ids);

            this.setState({ questionLoad: true, question: ids, type: id.type })
            this.GetQuestion(id._id)
        }
    }
    PreviousQuestion = () => {
        if (this.state.question != 1) {
            var ids = this.state.question - 1;
            var id = Question.Question.find(a => a.Id == this.state.question - 1);

            this.setState({ questionLoad: true, question: ids, type: id.type })
            this.GetQuestion(id._id)
        }
    }
    GetQuestionId = async (id) => {
        if (id != null) {
            // console.log(id)
            const ids = await Question.Question.find(a => a.Id == id);
            this.setState({ questionLoad: true, question: id, type: ids.type })
            // this.setState({ questionLoad: true, question: id,type:ids.type })
            this.GetQuestion(ids._id)
        }
    }
    componentDidMount() {
        var id = Question.Question.find(a => a.Id == this.state.question);
        this.GetQuestion(id._id)
        this.myInterval = setInterval(() => {
            const { seconds, minutes } = this.state

            if (seconds > 0) {
                this.setState(({ seconds }) => ({
                    seconds: seconds - 1
                }))
            }
            if (seconds === 0) {
                if (minutes === 0) {
                    clearInterval(this.myInterval)
                } else {
                    this.setState(({ minutes }) => ({
                        minutes: minutes - 1,
                        seconds: 59
                    }))
                }
            }
        }, 1000)
    }

    componentWillUnmount() {
        clearInterval(this.myInterval)
    }

    render() {
        const { IsModel } = this.state;
        const { minutes, seconds, questionLoad, Questions, question } = this.state
        const item = Questions;
        const CommonData = ({ type, title, std, subject, qust }) => {
            return (
                <>
                    <div className='d-md-flex align-items-center justify-content-between'>
                        <div style={{ flexBasis: '100%' }}>
                            <h6>
                                <span>{qust}</span>
                                {type}
                            </h6>
                            <h6>{qust != 4 ? title : ""}</h6>
                        </div>
                    </div>
                </>
            )
        }
        return (
            <div className="py-3" style={{ height: '100vh', backgroundColor: '#EDF6F7', overflow: 'auto' }}>
                <Container>

                    <div className="card shadow-sm border-0 mb-2">
                        <div className='card-header py-1 border-bottom border-light d-flex align-items-center justify-content-between' style={{ backgroundColor: '#fff' }}>

                            <div>
                                <img src="images/logo.png" width="220" />
                            </div>
                            <div className='d-flex align-items-end justify-content-end'>
                                <img src='https://icons.iconarchive.com/icons/custom-icon-design/silky-line-user/256/user-icon.png' width={30} className="me-2" />
                                <div>
                                    <h6 className='mb-0 small' style={{ lineHeight: 0 }}>Jhon Smith</h6>
                                    <span className="text-secondary" style={{ fontSize: 10, }}>jhon123@gmail.com</span>
                                </div>
                            </div>
                        </div>
                        <div className='card-body py-2'>
                            <div class="d-flex justify-content-between align-items-center">
                                <div>
                                    <p className="text-theme mb-0" style={{ fontWeight: 600, fontSize: 18 }}> Summer Test New - <span className="subject">Maths - 2B</span> </p>
                                </div>
                                <div className='d-flex align-items-center'>
                                    <p className='text-secondary'>Time Remaining  &nbsp;&nbsp;</p>
                                    <div className=''>
                                        {minutes === 0 && seconds === 0
                                            ? <p className='text-danger'>Time Over !</p>
                                            :
                                            <div className='d-flex justify-content-between timer-box'>
                                                <div className='green-box text-center'>
                                                    <span>00</span>
                                                    <small>Hrs</small>
                                                </div>
                                                <div className='green-box text-center'>
                                                    <span>{minutes}</span>
                                                    <small>Min</small>
                                                </div>
                                                <div className='green-box text-center'>
                                                    <span>{seconds < 10 ? `0${seconds}` : seconds}</span>
                                                    <small>Sec</small>
                                                </div>
                                            </div>
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {questionLoad ?
                        <div className='card shadow-sm border-0 questions-card title-q mb-2 d-flex justify-content-center align-items-center' style={{ height: 550 }}>
                            <ThreeDots
                                height="100"
                                width="100"
                                color='#EE4491'
                                ariaLabel='loading'
                            />
                        </div>
                        : item.QBMType == "A" ?
                            <div className='card shadow-sm border-0 questions-card title-q mb-2' style={{ height: 550, overflowY: 'auto' }}>
                                <div className='card-body p-md-4 p-3 '>
                                    {<CommonData type={this.state.type} title={item.QBMDetail.QBMDQuestionTitle} std={item.QBMStd} subject={item.QBMSubject} qust={question} />}
                                    <div className='d-flex align-items-center mb-3 ps-36'>
                                        <Form.Control type="text" placeholder="" className='txtbox w-25 me-2' name="ans" /> {item.QBMDetail.QBMDWordAfterBlank}
                                    </div>
                                </div>
                            </div> :
                            item.QBMType == "B" || item.QBMType == "BA" || item.QBMType == "BB" || item.QBMType == "BC" ?

                                <div className='card shadow-sm border-0 questions-card title-q mb-2' style={{ height: 550, overflowY: 'auto' }}>
                                    <div className='card-body p-md-4 p-3'>
                                        {<CommonData type={this.state.type} title={item.QBMDetail.QBMDQuestionTitle} std={item.QBMStd} subject={item.QBMSubject} qust={question} />}
                                        <Row>
                                            {item.QBMDetail.QBMDDirection == "Vertical"
                                                ?
                                                <Col md={12}>
                                                    <div className="ps-36">
                                                        <div className='mb-4'>
                                                            <span style={{ fontWeight: '500' }}>{item.QBMDetail.QBMDOperator == "-" ? "Subtract" : item.QBMDetail.QBMDOperator == "+" ?
                                                                "Addition" : item.QBMDetail.QBMDOperator == "x" ? "Multiplication" : item.QBMDetail.QBMDOperator == "/" ? "Division" : ""} :</span>
                                                        </div>
                                                        <div className='' style={{ width: 80 }}>
                                                            <div className='position-relative'>
                                                                <p className='mb-1 text-end' style={{ fontWeight: '500' }}>{item.QBMDetail.QBMDOperand1}</p>
                                                                <span className='arithmatic-operator'>{item.QBMDetail.QBMDOperator}</span><p className='mb-1 text-end' style={{ fontWeight: '500' }}>{item.QBMDetail.QBMDOperand2}</p>
                                                            </div>
                                                            <hr style={{ backgroundColor: '#000', opacity: 1 }} className='mb-2 mt-2' />
                                                            <Form.Control type="text" placeholder="" className='form-control form-control-sm mb-3 text-end' />

                                                        </div>
                                                    </div>
                                                </Col>
                                                :
                                                <Col md={12}>
                                                    <div className="ps-36">
                                                        <div className='mb-4'>
                                                            <span style={{ fontWeight: '500' }}>{item.QBMDetail.QBMDOperator == "-" ? "Subtract" : item.QBMDetail.QBMDOperator == "+" ?
                                                                "Addition" : item.QBMDetail.QBMDOperator == "x" ? "Multiplication" : item.QBMDetail.QBMDOperator == "/" ? "Division" : ""} :</span>
                                                        </div>
                                                        <div className='' style={{}}>
                                                            <div className='position-relative mb-4'>
                                                                <p className='mb-4' style={{ fontWeight: '500', fontSize: 20 }}>{item.QBMDetail.QBMDOperand1} <span>{item.QBMDetail.QBMDOperator}</span> {item.QBMDetail.QBMDOperand2} = <input type="text" placeholder="" className='txtbox border rounded' /></p>
                                                            </div>


                                                        </div>
                                                    </div>
                                                </Col>
                                            }
                                        </Row>
                                    </div>
                                </div> :
                                item.QBMType == "C" ?
                                    <div className='card shadow-sm border-0 questions-card title-q mb-2' style={{ height: 550, overflowY: 'auto' }}>
                                        <div className='card-body p-md-4 p-3 '>
                                            {<CommonData type={this.state.type} title={item.QBMDetail.QBMDQuestionTitle} std={item.QBMStd} subject={item.QBMSubject} qust={question} />}
                                            <Row>
                                                <Col md={6}>
                                                    <div className="ps-36">

                                                        <div className='d-flex mb-4'>
                                                            <div>
                                                                <div className='overlay-checkbox'>
                                                                    <form class="form cf" action="/" method="post">
                                                                        <section class="plan cf">
                                                                            {/* http://localhost:4000/uploads/question/bank/ */}
                                                                            {item.QBMDetail.QBMDOptions.map((img, key) => {
                                                                                return (
                                                                                    <div className='image-question'>
                                                                                        <input type="radio" name="img" id={img} value={"Option" + key} />
                                                                                        <label class="free-label four col" for={img}>
                                                                                            <Image src={Url.FileUrl() + img} className="img-fluid" />
                                                                                        </label>
                                                                                    </div>
                                                                                )
                                                                            })}
                                                                            {/* <div className='image-question'>
                                                                <input type="radio" name="img" id="img1" value="img1" />
                                                                <label class="free-label four col" for="img1">
                                                                    <Image src='/images/bar.png' />

                                                                </label>
                                                            </div>
                                                            <div className='image-question'>
                                                                <input type="radio" name="img" id="img2" value="img2" />
                                                                <label class="free-label four col" for="img2">
                                                                    <Image src='/images/bar2.png' />
                                                                </label>
                                                            </div> */}
                                                                        </section>

                                                                    </form>
                                                                </div>
                                                            </div>

                                                        </div>

                                                    </div>
                                                </Col>

                                            </Row>
                                        </div>
                                    </div> :
                                    item.QBMType == "D" ?
                                        <div className='card shadow-sm border-0 questions-card title-q mb-2' style={{ height: 550, overflowY: 'auto' }}>
                                            <div className='card-body p-md-4 p-3 '>
                                                {<CommonData type={this.state.type} title={item.QBMDetail.QBMDQuestionTitle} std={item.QBMStd} subject={item.QBMSubject} qust={question} />}
                                                <div className="ps-36">
                                                    <span className='text-success'>Read the passage</span><br />
                                                    <div className='bg-light p-md-3 p-2 mb-3'>
                                                        <Row>
                                                            <Col md={6}>
                                                                <div>
                                                                    <h6 className='text-center mb-1'>{item.QBMDetail.QBMDQuestionTitle}</h6>

                                                                    <p className='mb-0' dangerouslySetInnerHTML={{ __html: item.QBMDetail.QBMDPassage }}></p>
                                                                </div>
                                                            </Col>
                                                            <Col md={6}>
                                                                <div>

                                                                    <Image src={Url.FileUrl() + item.QBMDetail.QBMDPassageImage} className='img-fluid' style={{ height: 250, objectFit: "cover" }} />
                                                                </div>
                                                            </Col>
                                                        </Row>
                                                    </div>

                                                    <span className='text-success'>{item.QBMDetail.QBMDOptionTitle}</span><br />
                                                    <form class="form cf" action="/" method="post">
                                                        <section class="plan cf d-block">
                                                            {item.QBMDetail.QBMDOptions.map((o, key) => {
                                                                return (
                                                                    <div className={"mb-2"}>
                                                                        <input type="radio" name="img" id={"op" + o} value={"Option" + key} />
                                                                        <label class="free-label border-radio col" for={"op" + o}>
                                                                            {o}
                                                                        </label>
                                                                    </div>
                                                                )
                                                            })}
                                                            {/* <div>
                                        <input type="radio" name="img" id="img4" value="img4" />
                                        <label class="free-label border-radio col" for="img4">
                                            People who exercise regularly are happier.
                                        </label>
                                    </div> */}
                                                        </section>
                                                    </form>
                                                </div>
                                            </div>
                                        </div> : null

                    }


                    <div className='card shadow-sm border-0 questions-card'>
                        <div className='card-body p-md-3 p-3 '>
                            <div class="demo d-flex justify-content-between">
                                <div>
                                    <ul className="pagination next-prev justify-content-start">
                                        <li class="page-item">
                                            <a onClick={() => this.PreviousQuestion()} class="page-link" aria-label="Previous">
                                                <span aria-hidden="true">« Previous</span>
                                            </a>
                                        </li>
                                        <li class="page-item">
                                            <a onClick={() => this.NextQuestion()} class="page-link" aria-label="Next">
                                                <span aria-hidden="true">Next »</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>

                                <nav class="pagination-outer" aria-label="Page navigation">
                                    <ul class="pagination">
                                        <li onClick={() => this.GetQuestionId(1)} className={question == 1 ? "page-item active" : "page-item"}><a class="page-link">1</a></li>
                                        <li onClick={() => this.GetQuestionId(2)} className={question == 2 ? "page-item active" : "page-item"}><a class="page-link" >2</a></li>
                                        <li onClick={() => this.GetQuestionId(3)} class={question == 3 ? "page-item active" : "page-item"}><a class="page-link" >3</a></li>
                                        <li onClick={() => this.GetQuestionId(4)} class={question == 4 ? "page-item active" : "page-item"}><a class="page-link" >4</a></li>
                                        <li onClick={() => this.GetQuestionId(5)} class={question == 5 ? "page-item active" : "page-item"}><a class="page-link">5</a></li>
                                    </ul>
                                </nav>
                            
                                <div className=''>
                                <a href='/student/attendtest/01G92FDCGAZVN0E290QSMD40TV'  className='btn btn-secondary me-1'>Cancel</a>
                                    <button onClick={() => { this.setState({ IsModel: true }) }} className='btn btn-theme'>Submit Test</button>
                                </div>

                                <Modal size='' centered show={IsModel} onHide={() => { this.setState({ IsModel: false }) }}>
                                    <Modal.Header closeButton>
                                        <Modal.Title>
                                            <h5 className="mb-0">Confirmation</h5>
                                        </Modal.Title>
                                    </Modal.Header>
                                    <Modal.Body>
                                        <div className="text-center">
                                            <img src="images/success.png" width={80} className="d-block mx-auto mb-3" />
                                            <p style={{fontSize:18,fontWeight:600}}>Are you sure <br /> you want to Submit this test.</p>
                                            <a href="/student/test/success" className="btn btn-success me-2">Confirm</a>
                                            <a  onClick={() => { this.setState({ IsModel: false }) }} className="btn btn-danger">Cancel</a>
                                        </div>
                                    </Modal.Body>

                                </Modal>

                            </div>


                        </div>

                    </div>
                </Container>
            </div>
        )
    }
}
