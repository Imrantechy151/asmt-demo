import React, { Component } from 'react';
import { Container, Row, Col } from 'react-bootstrap';


export default class AttendTest extends Component {
  render() {
    return (
      <div>
        <Row>
          <Col md={12}>            
            <div className="card p-3 shadow-sm border-0">
              <div class="d-flex justify-content-between align-items-center mb-3">
                <div>
                  <p className="text-dark mb-0" style={{ fontWeight: 600, fontSize: 20 }}> Summer Test New  - Maths - 2B</p>
                  <p className='text-secondary small mb-0'>Created by <span className="text-success">Mr Jhon smith</span></p>
                </div>
                <div className='d-flex align-items-center border p-2 rounded bg-light'>
                  <img src='https://icons.iconarchive.com/icons/google/noto-emoji-travel-places/1024/42603-hourglass-done-icon.png' width={35} className="me-1" />
                  <div>
                    <p className='mb-0 text-secondary' style={{ lineHeight: 1, fontSize: 12 }}>Duration</p>
                    <span className='text-success small' style={{ fontWeight: '600' }}>30 Minutes</span>
                  </div>
                </div>
              </div>
              
              <div className='note mt-3'>
                <h6 className='text-danger' style={{ fontWeight: 600 }}>Note :</h6>
                <ul className='px-3'>
                  <li>You should close all other applications that may distrupt.</li>
                  <li>You have to finish the test without any interruption.</li>
                  <li>Minus method on this test (one question wrong cut minus .25 marks).</li>
                </ul>
              </div>
              <div className='mt-4 text-center'>
                <a href='/starttest' className="btn btn-theme">Start Exam</a>                
              </div>
            </div>
          
           

          </Col>
        </Row>

      </div>
    )
  }
}
