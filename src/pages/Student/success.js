import React, { Component } from 'react'

export default class success extends Component {
  render() {
    return (
      <div className='card shadow-sm rounded border-0 p-3 py-5 text-center'>
        <img src='/images/success.png' className='d-block mx-auto mb-3' width={90} />
        <p className='text-secondary'>Your test has been successfully submitted.<br/>
        Click here for view test result.</p>
        
        <div>
            <a href='/student/test/result' className='btn btn-theme btn-sm mt-4'>View Your Result</a>
        </div>          
      </div>
    )
  }
}
