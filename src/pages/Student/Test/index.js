import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import 'font-awesome/css/font-awesome.min.css';
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import ApiService from '../../../services/ApiServices';
import { ThreeDots } from 'react-loader-spinner'
import Data from '../../../helper/data.json';

export default class Index extends Component {
  constructor(props) {
    super(props)
    this.state = {
      type: null, subject: null,
      date: new Date(), student: null,
      std: null,
      Duration: null,
      quiz: [],
      isLoading: false,
      isSubmit: false,
      count: 0
    }
  }
  async componentDidMount() {
    await ApiService.GetExam().then(res => {
      console.log(res.data);
      let result = res.data;
      if (result.status == 200) {
        this.setState({ quiz: result.response, isLoading: false })
      }
    }, error => {
      console.log(error);
    })
  }
  render() {
    const { quiz, isLoading, details, count, } = this.state;
    let flag = 1;
    if (isLoading) {
      return (
        <div style={{ position: "fixed", left: "50%", top: "30%", bottom: 0, right: "50%", width: "100%", zIndex: 9999 }}>
          <ThreeDots
            height="100"
            width="100"
            color='#EE4491'
            ariaLabel='loading'
          />
        </div>
      )
    }
    return (
      <div>
        <div className='card p-3 py-2 shadow-sm border-0'>
          <div className='text-end d-md-flex justify-content-between align-items-center'>
              <div className='d-flex align-items-center'>
                <h5 className='mb-0 text-secondary me-3'>Test List</h5>
                <div className='d-flex align-items-center gap-1'>
                  <span className=''><small className='btn btn-outline-secondary btn-sm py-0 fw-normal'>Total : 5</small> </span>
                  <span className=''><small className='btn btn-outline-success btn-sm py-0 fw-normal'> Completed : 4</small></span>
                  <span className=''><small className='btn btn-outline-primary btn-sm py-0 fw-normal'>Pending : 1</small></span> 
                </div>                  
              </div>              
              <div className='d-flex align-items-center' >               
                <select aria-label="Default select example" class="form-select form-select-sm ms-4" id="formBasicEmail" style={{height:35}}>                  
                  <option value="Easy">All</option>
                  <option value="Medium">Completed</option>
                  <option value="Hard">Pending</option>
                </select>
              </div>
          </div>
        </div>

        <div className='mt-3'>
          {quiz.map((details, key) => {
            console.log(details)
            if (flag == 1) {
              flag = 0;
            } else {
              flag = 1;
            }
            return (
              <div className='card p-md-3 p-3 shadow-sm border-0 mb-2'>
                

                <div className='d-md-flex justify-content-between align-items-center'>

                  <div>
                    <p className='text-theme mb-1' style={{ fontWeight: '600', fontSize: 18 }}> {details.EMName}</p>
                    <p className='text-secondary small mb-0'>Created by  <span className='' style={{ fontWeight: '600' }}>James Anderson</span></p>
                    <small className='text-secondary'>Start Date : 07-07-2022 &nbsp;| End Date : 27-07-2022</small>
                    {flag==1?
                    <a onClick={() => {
                      window.localStorage.setItem("exmId", details._id)
                    }} href={"/student/attendtest/" + details._id} className='btn btn-outline-primary btn-sm ms-2 text-decoration-none'>Start Exam</a>
               :
               <a href='/student/test/result' className='btn btn-outline-success btn-sm ms-2 text-decoration-none'>View Result</a>
                    }
                    </div>

                  <div className='' style={{ width: '20%' }}>

                    <div className='d-flex align-items-center position-relative'>

                      <div className='bg-color2 text-center me-2 rounded py-2' style={{ flexBasis: 180 }}>
                        <span className='fw-normal text-start' style={{ fontSize: 12 }}>Subject</span>
                        <p className='mb-0' style={{ fontSize: 13, fontWeight: '600' }}>{details.EMSubject}</p>
                      </div>

                      <div className='bg-color3 text-center me-2 rounded py-2' style={{ flexBasis: 180 }}>
                        <span className='fw-normal text-start' style={{ fontSize: 12 }}>New Duration</span>
                        <p className='mb-0' style={{ fontSize: 12, fontWeight: '600' }}>{details.EMDuration}</p>
                      </div>
                    

                    </div>
                  </div>

                </div>

               

              </div>
            )
          })}
        </div>
      </div>
    )
  }
}
