import React, { Component } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import List from './testlist';

export default class result extends Component {
    render() {
        return (
            <div>
              
                            <div className='card shadow-sm border-0'>
                                <div className='card-header py-3 border-bottom border-light d-flex align-items-center justify-content-between' style={{ backgroundColor: '#fbfbfb' }}>
                                    <h6 className='mb-0 text-theme' style={{ fontSize: 17 }}>Trivia Quiz Quest</h6>
                                </div>
                                <div className='card-body p-4'>

                                    <ul className='result-table'>
                                        <li className='bg-color1'>
                                            <div>
                                                <small>Attendant Date</small>
                                                <p>17/07/2022</p>
                                            </div>                                            
                                        </li>
                                        <li>
                                            <div>
                                                <small>Subject</small>
                                                <p>Maths</p>
                                            </div>                                            
                                        </li>
                                        <li>
                                            <div>
                                                <small>Year Level</small>
                                                <p>2</p>
                                            </div>                                            
                                        </li>
                                        <li>
                                            <div>
                                                <small>Class</small>
                                                <p>2A</p>
                                            </div>                                            
                                        </li>
                                        <li className='border-0'>
                                            <div className='card border-0 rounded'>
                                                <div className='bg-color1 p-4 text-center rounded'>
                                                    <h6 className='mb-3'>Number of Questions</h6>
                                                    <b className='text-white badge bg-info'>5</b>
                                                </div>
                                            </div>
                                        </li>
                                        <li className='card w-100'>                                            
                                            <div className='bg-color2 w-100 p-3 text-center border-0 rounded'>
                                                <h6 className='mb-0'>Right</h6>
                                            </div>
                                            <div className='card-body py-4 text-center'>
                                                <b className='text-success'><i className="fa fa-check text-success"></i> 4</b>
                                            </div>                                            
                                        </li>
                                        <li className='card w-100'>                                           
                                                <div className='bg-color3 w-100 p-3 text-center border-0 rounded'>
                                                    <h6 className='mb-0'>Wrong</h6>
                                                </div>
                                                <div className='card-body py-4 text-center'>
                                                    <b className='text-danger'><i className="fa fa-times text-danger"></i> 1</b>
                                                </div>                                            
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        
                <List/>
            </div>
        )
    }
}
