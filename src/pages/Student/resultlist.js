import React, { Component } from 'react'
import { Button, Col, Form, Image, Row, Modal } from 'react-bootstrap'
import Url from '../../helper/url';
import Types from '../../helper/type.json'
import Question from '../../helper/question.json'


export default class list extends Component {
    constructor(props) {
        super(props)
        this.state = {
            IsModel: false,
            explanation: ""
        }
    }
    render() {
        const { data } = this.props
        const { IsModel } = this.state;
        let SRNo = 0;
        const CommonData = ({ type, title, std, subject }) => {
            return (
                <>
                    <div className='d-md-flex align-items-center justify-content-between'>
                        <div style={{ flexBasis: '80%' }}>
                            <h6>
                                {type == "D" ? "Determine the main idea of a passage" : null}
                                {type == "C" ? "Place value models - tens and ones" : title}
                            </h6>
                        </div>
                    </div>
                </>
            )
        }
        const CommonData1 = ({ Number, type, std, subject, code }) => {
            var tp = Types.find(a => a.value == type);
            return (
                <div className='card-header d-md-flex justify-content-between bg-white p-3' style={{ borderBottomColor: '#f0f0f0' }}>
                    <div>                       
                        <span className='badge btn-theme text-white fw-normal me-2' data-bs-toggle="tooltip" title='Question Type'>{tp ? tp.name : ""}</span>
                    </div>
                    <div>

                        <a href='#' className='px-2 text-decoration-none small'>
                            {type == "B" ? <div className="right-q bg-danger"><i className='fa fa-times'></i></div> : <div className="right-q bg-success"><i className='fa fa-check'></i></div>}

                        </a>
                    </div>
                </div>
            )
        }

        const CommonData2 = ({ type, title, std, subject, timer, explanation }) => {
            return (
                <div className='card-footer px-4 d-md-flex align-items-center justify-content-between p-3 border-0' style={{ backgroundColor: '#f7fcfc' }}>
                    <div>
                        <span className='btn-link text-primary text-decoration-none small' onClick={() => { this.setState({ IsModel: true, explanation: explanation }) }} style={{ color: 'gray', cursor: 'pointer' }}>View Explanation  </span> &nbsp;
                    </div>

                </div>
            )
        }

        return (
            <div>
                <Modal size='lg' show={IsModel} onHide={() => { this.setState({ IsModel: false }) }}>
                    <Modal.Header closeButton>
                        <Modal.Title>Explanation</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>

                        <div dangerouslySetInnerHTML={{ __html: this.state.explanation }}></div>
                    </Modal.Body>

                </Modal>
                {data.map((item) => {
                    let ques = Question.Question.find(a => a._id == item._id);

                    if (ques == null) {
                        return null;
                    }
                    SRNo = SRNo + 1;
                    if (item.QBMType == "A") {
                        return (
                            <>
                                <div className={item.QBMType=="B"?'card shadow-sm border-danger questions-card':'card shadow-sm border-success questions-card'}>
                                    {<CommonData1 Number={SRNo} type={item.QBMType} std={item.QBMStd} subject={item.QBMSubject} code={item._id} />}
                                    <div className='card-body p-md-4 p-3 '>
                                        {<CommonData title={item.QBMDetail.QBMDQuestionTitle} std={item.QBMStd} subject={item.QBMSubject} />}
                                        <div className='d-flex align-items-center mb-3'>
                                            <Form.Control type="text" placeholder="" className='txtbox' name="ans" /> {item.QBMDetail.QBMDWordAfterBlank}
                                        </div>
                                    </div>
                                    {<CommonData2 explanation={item.QBMExplanation} timer={item.QBMTimerValue} />}
                                </div>
                            </>
                        )
                    }
                    else if (item.QBMType == "B" || item.QBMType == "BA" || item.QBMType == "BB" || item.QBMType == "BC") {
                        return (
                            <div className={item.QBMType=="B"?'card shadow-sm border-danger questions-card':'card shadow-sm border-success questions-card'}>
                                {/* <h6>
                                    <span>&#x25B7;</span> {item.QBMDetail.QBMDQuestionTitle}
                                </h6> */}
                                {<CommonData1 Number={SRNo} type={item.QBMType} std={item.QBMStd} subject={item.QBMSubject} code={item._id} />}
                                <div className='card-body p-md-4 p-3 '>
                                    {<CommonData title={item.QBMDetail.QBMDQuestionTitle} std={item.QBMStd} subject={item.QBMSubject} />}
                                    <Row>
                                        {item.QBMDetail.QBMDDirection == "Vertical"
                                            ?
                                            <Col md={12}>
                                                <div>
                                                    <div className='mb-4'>
                                                        <span style={{ fontWeight: '500' }}>{item.QBMDetail.QBMDOperator == "-" ? "Subtract" : item.QBMDetail.QBMDOperator == "+" ?
                                                            "Addition" : item.QBMDetail.QBMDOperator == "x" ? "Multiplication" : item.QBMDetail.QBMDOperator == "/" ? "Division" : ""} :</span>
                                                    </div>
                                                    <div className='ms-3' style={{ width: 80 }}>
                                                        <div className='position-relative'>
                                                            <p className='mb-1 text-end' style={{ fontWeight: '500' }}>{item.QBMDetail.QBMDOperand1}</p>
                                                            <span className='arithmatic-operator'>{item.QBMDetail.QBMDOperator}</span><p className='mb-1 text-end' style={{ fontWeight: '500' }}>{item.QBMDetail.QBMDOperand2}</p>
                                                        </div>
                                                        <hr style={{ backgroundColor: '#000', opacity: 1 }} className='mb-2 mt-2' />
                                                        <Form.Control type="text" placeholder="" className='form-control form-control-sm mb-3 text-end' />

                                                    </div>
                                                </div>
                                            </Col>
                                            :
                                            <Col md={12}>
                                                <div>
                                                    <div className='mb-4'>
                                                        <span style={{ fontWeight: '500' }}>{item.QBMDetail.QBMDOperator == "-" ? "Subtract" : item.QBMDetail.QBMDOperator == "+" ?
                                                            "Addition" : item.QBMDetail.QBMDOperator == "x" ? "Multiplication" : item.QBMDetail.QBMDOperator == "/" ? "Division" : ""} :</span>
                                                    </div>
                                                    <div className='ms-3' style={{}}>
                                                        <div className='position-relative mb-4'>
                                                            <p className='mb-4' style={{ fontWeight: '500', fontSize: 20 }}>{item.QBMDetail.QBMDOperand1} <span>{item.QBMDetail.QBMDOperator}</span> {item.QBMDetail.QBMDOperand2} = <input type="text" placeholder="" className='txtbox border rounded' /></p>
                                                        </div>


                                                    </div>
                                                </div>
                                            </Col>
                                        }
                                    </Row>

                                </div>
                                {<CommonData2 explanation={item.QBMExplanation} timer={item.QBMTimerValue} />}
                            </div>
                        )
                    }
                    else if (item.QBMType == "C") {
                        return (
                            <div className={item.QBMType=="B"?'card shadow-sm border-danger questions-card':'card shadow-sm border-success questions-card'}>
                                {/* <h6>
                                    <span>&#x25B7;</span> Place value models - tens and ones
                                </h6> */}
                                {<CommonData1 Number={SRNo} type={item.QBMType} std={item.QBMStd} subject={item.QBMSubject} code={item._id} />}
                                <div className='card-body p-md-4 p-3 '>
                                    {<CommonData type={item.QBMType} title={item.QBMDetail.QBMDQuestionTitle} std={item.QBMStd} subject={item.QBMSubject} />}
                                    <Row>
                                        <Col md={6}>
                                            <div>
                                                <div className='mb-4'>
                                                    <span style={{ fontWeight: '500' }}>{item.QBMDetail.QBMDQuestionTitle}</span>
                                                </div>
                                                <div className='d-flex mb-4'>
                                                    <div>
                                                        <div className='overlay-checkbox'>
                                                            <form class="form cf" action="/" method="post">
                                                                <section class="plan cf">
                                                                    {/* http://localhost:4000/uploads/question/bank/ */}
                                                                    {item.QBMDetail.QBMDOptions.map((img, key) => {
                                                                        return (
                                                                            <div className='image-question'>
                                                                                <input type="radio" name="img" id={img} value={"Option" + key} />
                                                                                <label class="free-label four col" for={img}>
                                                                                    <Image src={Url.FileUrl() + img} className="img-fluid" />
                                                                                </label>
                                                                            </div>
                                                                        )
                                                                    })}
                                                                    {/* <div className='image-question'>
                                                                    <input type="radio" name="img" id="img1" value="img1" />
                                                                    <label class="free-label four col" for="img1">
                                                                        <Image src='/images/bar.png' />

                                                                    </label>
                                                                </div>
                                                                <div className='image-question'>
                                                                    <input type="radio" name="img" id="img2" value="img2" />
                                                                    <label class="free-label four col" for="img2">
                                                                        <Image src='/images/bar2.png' />
                                                                    </label>
                                                                </div> */}
                                                                </section>

                                                            </form>
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>
                                        </Col>

                                    </Row>
                                </div>
                                {<CommonData2 explanation={item.QBMExplanation} timer={item.QBMTimerValue} />}

                            </div>
                        );
                    }
                    else if (item.QBMType == "D") {
                        return (
                            <div className={item.QBMType=="B"?'card shadow-sm border-danger questions-card':'card shadow-sm border-success questions-card'}>
                                {/* <h6>
                                <span>&#x25B7;</span> Place value models - tens and ones
                            </h6> */}
                                {<CommonData1 Number={SRNo} type={item.QBMType} std={item.QBMStd} subject={item.QBMSubject} code={item._id} />}
                                <div className='card-body p-md-4 p-3 '>

                                    {<CommonData type={item.QBMType} title={item.QBMDetail.QBMDQuestionTitle} std={item.QBMStd} subject={item.QBMSubject} />}
                                    <span className='text-success'>Read the passage</span><br />
                                    <div className='bg-light p-md-3 p-2 mb-3'>
                                        <Row>
                                            <Col md={6}>
                                                <div>
                                                    <h6 className='text-center mb-1'>{item.QBMDetail.QBMDQuestionTitle}</h6>

                                                    <p className='mb-0' dangerouslySetInnerHTML={{ __html: item.QBMDetail.QBMDPassage }}></p>
                                                </div>
                                            </Col>
                                            <Col md={6}>
                                                <div>

                                                    <Image src={Url.FileUrl() + item.QBMDetail.QBMDPassageImage} className='img-fluid' style={{ height: 250, objectFit: "cover" }} />
                                                </div>
                                            </Col>
                                        </Row>
                                    </div>
                                    <span className='text-success'>{item.QBMDetail.QBMDOptionTitle}</span><br />
                                    <form class="form cf" action="/" method="post">
                                        <section class="plan cf d-block">
                                            {item.QBMDetail.QBMDOptions.map((o, key) => {
                                                return (
                                                    <div className={"mb-2"}>
                                                        <input type="radio" name="img" id={"op" + o} value={"Option" + key} />
                                                        <label class="free-label border-radio col" for={"op" + o}>
                                                            {o}
                                                        </label>
                                                    </div>
                                                )
                                            })}
                                            {/* <div>
                                        <input type="radio" name="img" id="img4" value="img4" />
                                        <label class="free-label border-radio col" for="img4">
                                            People who exercise regularly are happier.
                                        </label>
                                    </div> */}
                                        </section>
                                    </form>

                                </div>
                                {<CommonData2 explanation={item.QBMExplanation} timer={item.QBMTimerValue} />}

                            </div>
                        )
                    }
                    else {
                        return null
                    }
                })}
            </div>
        )
    }
}
