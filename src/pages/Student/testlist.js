import React, { Component } from 'react'
import { Button, Table, Dropdown,Row,Col,Form } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import 'font-awesome/css/font-awesome.min.css';
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import ApiService from '../../services/ApiServices';
import { ThreeDots } from 'react-loader-spinner'
import List from './resultlist';
import Question from '../../helper/question.json'


export default class QuizList extends Component {
  constructor(props) {
    super(props)
    this.state = {
      quiz: [],
      isLoading: true
    }
  }
  async componentDidMount() {
    await ApiService.GetQuestionBank().then(res => {
      console.log(res.data);
     
      let result = res.data;
      if (result.status == 200) {
        this.setState({ quiz: result.response, isLoading: false })
      }
    }, error => {
      console.log(error);
    })
  }
  render() {
    const { quiz, isLoading } = this.state;
    if (isLoading) {
      return (
        <div style={{ position: "fixed", left: "50%", top: "30%", bottom: 0, right: "50%", width: "100%", zIndex: 9999 }}>
          <ThreeDots
            height="100"
            width="100"
            color='#EE4491'
            ariaLabel='loading'
          />
        </div>
      )
    }
    return (
      <div>        
        <div className='mt-3'>
          <List data={quiz} />
        </div>
      </div>
    )
  }
}
