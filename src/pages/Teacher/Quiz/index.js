import React, { Component } from 'react'
import { Button, Table, Dropdown,Row,Col,Form } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import 'font-awesome/css/font-awesome.min.css';
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import ApiService from '../../../services/ApiServices';
import { ThreeDots } from 'react-loader-spinner'
import List from './list';
import Type from '../../../helper/type.json';

export default class QuizList extends Component {
  constructor(props) {
    super(props)
    this.state = {
      quiz: [],
      isLoading: true
    }
  }
  async componentDidMount() {
    await ApiService.GetQuestionBank().then(res => {
      console.log(res.data);
      let result = res.data;
      if (result.status == 200) {
        this.setState({ quiz: result.response, isLoading: false })
      }
    }, error => {
      console.log(error);
    })
  }
  render() {
    const { quiz, isLoading } = this.state;
    if (isLoading) {
      return (
        <div style={{ position: "fixed", left: "50%", top: "30%", bottom: 0, right: "50%", width: "100%", zIndex: 9999 }}>
          <ThreeDots
            height="100"
            width="100"
            color='#EE4491'
            ariaLabel='loading'
          />
        </div>
      )
    }
    return (
      <div>
      <div className='sticky-header'>
        <div className='card p-3 py-2 shadow-sm border-0 mb-2'>
          <div className='text-end d-md-flex justify-content-between align-items-center'>
            <h5 className='mb-0 text-secondary'>Question List</h5>
            <Link to='/teacher/question/create' className='btn btn-theme btn-sm'>Create New Question</Link>
          </div>          
        </div>
        <div className='card p-3 shadow-sm border-0'>
            <h5 className='mb-2 text-secondary'>Filter Question</h5>
            <Row>
              <Col md={3}>
                <Form.Group className="" controlId="formBasicEmail">
                  <Form.Label>Year Level</Form.Label>
                  <Form.Select>
                    <option>Select year level</option>
                    <option>5</option>
                    <option>6</option>
                    <option>7</option>
                  </Form.Select>                
                </Form.Group>
              </Col>
              <Col md={3}>
                <Form.Group className="" controlId="formBasicEmail">
                  <Form.Label>Subject</Form.Label>
                  <Form.Select>
                    <option>Select Subject</option>
                    <option>English</option>
                    <option>Maths</option>
                    <option>Science</option>
                  </Form.Select>                
                </Form.Group>
              </Col>
              <Col md={3}>
                <Form.Group className="" controlId="formBasicEmail">
                  <Form.Label>Question Type</Form.Label>
                  <Form.Select>
                    <option>Select question type</option>
                    {Type.map((item)=>
                      {
                        return (
                          <option value={item.value}>{item.id} - {item.name}</option>
                        );
                      }
                      )}       
                  </Form.Select>                
                </Form.Group>
              </Col>
              <Col md={3}>
                <Form.Group className="" controlId="formBasicEmail">
                  <Form.Label>Toughness Level</Form.Label>
                  <Form.Select>
                    <option>Select toughness level</option>
                    <option>Easy</option>
                    <option>Medium</option>
                    <option>Hard</option>
                  </Form.Select>                
                </Form.Group>
              </Col>
            </Row>        
        </div>
        </div>
        <div className='mt-3'>
          <List data={quiz} />
        </div>
      </div>
    )
  }
}
