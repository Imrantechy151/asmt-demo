import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import 'font-awesome/css/font-awesome.min.css';
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import ApiService from '../../../services/ApiServices';
import { ThreeDots } from 'react-loader-spinner'
import Data from '../../../helper/data.json';

export default class Index extends Component {
  constructor(props) {
    super(props)
    this.state = {
      type: null, subject: null,
      date: new Date(), student: null,
      std: null,
      Duration: null,
      quiz: [],
      isLoading: false,
      isSubmit: false,
      count: 0
    }
  }
  async componentDidMount() {
    await ApiService.GetExam().then(res => {
      console.log(res.data);
      let result = res.data;
      if (result.status == 200) {
        this.setState({ quiz: result.response, isLoading: false })
      }
    }, error => {
      console.log(error);
    })
  }
  render() {
    const { quiz, isLoading, details, count, } = this.state;
    let flag = 1;
    if (isLoading) {
      return (
        <div style={{ position: "fixed", left: "50%", top: "30%", bottom: 0, right: "50%", width: "100%", zIndex: 9999 }}>
          <ThreeDots
            height="100"
            width="100"
            color='#EE4491'
            ariaLabel='loading'
          />
        </div>
      )
    }
    return (
      <div>
        <div className='card p-3 shadow-sm border-0'>

          <div className='text-end d-md-flex justify-content-between'>
            <h5 className='mb-0 text-secondary'>Test List</h5>
            <Link to='/Teacher/Test/create' className='btn btn-theme btn-sm'>Create New Test</Link>
          </div>




        </div>
        <div className='mt-3'>
          {quiz.map((details, key) => {
            console.log(details)
            if (flag == 1) {
              flag = 0;
            } else {
              flag = 1;
            }
            return (
              <div className='card p-md-3 p-3 shadow-sm border-0 mb-2'>
                <a onClick={() => {
                  window.localStorage.setItem("exmId", details._id)
                }} href={"/Teacher/Test/Details/" + details._id} class="stretched-link"></a>
                <div className='d-md-flex justify-content-between align-items-center'>

                  <div>
                    <p className='text-theme mb-1' style={{ fontWeight: '600', fontSize: 20 }}> {details.EMName}</p>
                    <small className='text-secondary'>Created by  <span className='' style={{ fontWeight: '600' }}>James Anderson</span></small> &nbsp;<small>|</small>&nbsp;
                    <small className='text-secondary'>Created on date 07-07-2022</small>
                    <p className='mb-0 small text-theme mt-1 fw-normal'>Student Selected (50) &nbsp; Student Attend (45)</p>                   
                  </div>

                  <div className='' style={{ width: '45%' }}>

                    <div className='d-flex align-items-center position-relative'>

                      <div className='bg-color6 text-center me-2 rounded py-2' style={{ flexBasis: 180 }}>
                        <span className='fw-normal text-start' style={{ fontSize: 12 }}>Year Level</span>
                        <p className='mb-0' style={{ fontSize: 13, fontWeight: '700' }}>{details.EMStd}</p>
                      </div>

                      <div className='bg-color4 text-center me-2 rounded py-2' style={{ flexBasis: 180 }}>
                        <span className='fw-normal text-start' style={{ fontSize: 12 }}>Class</span>
                        <p className='mb-0' style={{ fontSize: 13, fontWeight: '700' }}>{details.EMStd}{details.EMClass}</p>
                      </div>

                      <div className='bg-color2 text-center me-2 rounded py-2' style={{ flexBasis: 180 }}>
                        <span className='fw-normal text-start' style={{ fontSize: 12 }}>Subject</span>
                        <p className='mb-0' style={{ fontSize: 13, fontWeight: '600' }}>{details.EMSubject}</p>
                      </div>

                      <div className='bg-color3 text-center me-2 rounded py-2' style={{ flexBasis: 180 }}>
                        <span className='fw-normal text-start' style={{ fontSize: 12 }}>Duration</span>
                        <p className='mb-0' style={{ fontSize: 13, fontWeight: '600' }}>{details.EMDuration}</p>
                      </div>
                      <div className='bg-color1 text-center me-2 rounded py-2' style={{ flexBasis: 180 }}>
                        <span className='fw-normal text-start' style={{ fontSize: 12 }}>Questions</span>
                        <p className='mb-0' style={{ fontSize: 13, fontWeight: '600' }}>{details.EMQuestionCount}</p>
                      </div>

                    </div>
                  </div>

                </div>

                <a href='#' className='px-2 text-decoration-none position-absolute' title='Edit' style={{ right: 5, top: 5 }}>
                  <i className='fa fa-pencil'></i>
                </a>

              </div>
            )
          })}
        </div>
      </div>
    )
  }
}
