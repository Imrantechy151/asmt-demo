import React, { Component } from 'react'

export default class NotFound extends Component {
  render() {
    return (
      <h3 className='text-center text-danger '>Not Found</h3>
    )
  }
}
